package com.tieba.common.utils;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.tieba.entity.TbUser;

/**
 * 保存和获取当前用户的工具类 Created by hugo on 2020/1/16.
 */
public class UserContext {

	/**
	 * 日志组件
	 */
	private static final Logger logger = LoggerFactory.getLogger(UserContext.class);

	private static final String CURRENT_USER_IN_SESSION = "logininfo_";

	/**
	 * 得到session
	 */
	private static HttpSession getSession() {
		// SpringMVC获取session的方式通过RequestContextHolder
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getSession();
	}

	/**
	 * 设置当前用户到session中
	 */
	public static void putCurrebtUser(TbUser currentUser) {
		String id = getSession().getId();
		logger.info("保存当前用户的sessionId：{}", id);
		getSession().setAttribute(CURRENT_USER_IN_SESSION + id, currentUser);
	}

	/**
	 * 获取当前用户
	 */
	public static TbUser getCurreentUser() {
		String id = getSession().getId();
		logger.info("获取当前用户的sessionId：{}", id);
		return (TbUser) getSession().getAttribute(CURRENT_USER_IN_SESSION + id);
	}

	/**
	 * Description: 退出登录时，删除用户信息 <br>
	 *
	 * @author liutian<br>
	 * @taskId <br>
	 * @Param []  <br>
	 * @return void <br>
	 * @CreateDate 2020/4/9 21:40 <br>
	 */
	public static void removeUser() {
		String id = getSession().getId();
		logger.info("删除当前用户的sessionId：{}", id);
		getSession().removeAttribute(CURRENT_USER_IN_SESSION + id);
	}
}
