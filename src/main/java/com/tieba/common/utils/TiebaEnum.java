/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.common.utils;

/**
 * <Description> <br>
 * 
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 23, 2020 8:50:51 PM <br>
 */

public enum TiebaEnum {

	POST_TYPE_GOV(1, "官方贴"), // 官方贴
	POST_TYPE_PALIN(2, "普通贴"), // 普通贴

	SHOW(1, "可以展示"), // 可以展示
	HIDE(0, "不会展示，为已删除"), // 不会展示，为已删除

	GROUPUSERTYPE_MAIN(1, "群主"), // 群主
	GROUPUSERTYPE_ADMIN(2, "群管理员"), // 管理员
	GROUPUSERTYPE_USER(3, "群员"), // 普通用户
	
	USER_TYPE_ADMIN(1, "管理员"), // 管理员
	USER_TYPE_GROUP(2, "团体用户"), // 团体用户
	USER_TYPE_USER(3, "普通用户"), // 普通用户
	
	POST_NOT_ISTOP(0, "不置顶"), // 不置顶
	POST_ISTOP(0, "置顶"), // 不置顶
	
	;

	private int type;

	private String typeName;

	TiebaEnum(int type, String typeName) {
		this.type = type;
		this.typeName = typeName;
	}

	public int getType() {
		return type;
	}

	public String getTypeName() {
		return typeName;
	}

}
