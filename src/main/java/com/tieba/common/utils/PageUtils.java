/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.common.utils;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <Description> <br>
 * 
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 21, 2020 10:58:38 PM <br>
 */

public class PageUtils {
	

	/**
	 * Description: 分页查询参数<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param request
	 * @return <br>
	 * @CreateDate Mar 21, 2020 11:00:44 PM <br>
	 */
	public static <T> Page<T> convertPage(HttpServletRequest request) {
		String page = request.getParameter("page");
		page = page != null && !"".equals(page) ? page : "1";
		String limit = request.getParameter("limit");
		limit = limit != null && !"".equals(limit) ? limit : "20";
		int pageInt = Integer.parseInt(page);
		int limitInt = Integer.parseInt(limit);
		Page<T> paramPage = new Page<>(pageInt, limitInt);
		return paramPage;
	}

}
