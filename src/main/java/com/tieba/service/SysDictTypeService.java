package com.tieba.service;

import com.tieba.entity.SysDictType;
import com.tieba.utils.ApiJsonResult;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface SysDictTypeService extends IService<SysDictType> {
	
	
	/**
	 * Description: 根据类型获取字典信息<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param type
	 * @return <br>
	 * @CreateDate Mar 22, 2020 10:39:58 PM <br>
	 */
	public ApiJsonResult<?> getDictByType(String type);
	
	/**
	 * Description: 根据类型和键值信息查询字典信息<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param type
	 * @param value
	 * @return <br>
	 * @CreateDate Mar 22, 2020 10:44:53 PM <br>
	 */
	public ApiJsonResult<?> getDictByType(String type, String value);

	/**
	 * Description: <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param sysDictPage
	 * @param sysDictType
	 * @return <br>
	 * @CreateDate Apr 1, 2020 11:25:27 PM <br>
	 */ 
	public ApiJsonResult<?> queryList(Page<SysDictType> sysDictPage, SysDictType sysDictType);

}
