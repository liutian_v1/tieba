package com.tieba.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tieba.entity.TbCollect;
import com.tieba.utils.ApiJsonResult;

/**
 * <p>
 * 收藏表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-27
 */
public interface TbCollectService extends IService<TbCollect> {
	
	/**
	 * Description: 添加收藏<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbCollect
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:35:16 PM <br>
	 */
	public ApiJsonResult<?> addCollect(TbCollect tbCollect);
	
	/**
	 * Description: 取消收藏<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbCollect
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:35:26 PM <br>
	 */
	public ApiJsonResult<?> cancleCollect(String collectId);
	
	/**
	 * Description: 收藏列表查询<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param pageInfo
	 * @param tbCollect
	 * @return <br>
	 * @CreateDate Apr 7, 2020 10:28:04 PM <br>
	 */
	public ApiJsonResult<?> queryCollectList(Page<TbCollect> pageInfo, TbCollect tbCollect);

}
