package com.tieba.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tieba.entity.TbPostInfo;
import com.tieba.utils.ApiJsonResult;

/**
 * <p>
 * 帖子表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbPostInfoService extends IService<TbPostInfo> {
	
	
	/**
	 * Description: 查询帖子列表<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param paramPage
	 * @return <br>
	 * @CreateDate Mar 22, 2020 11:09:04 PM <br>
	 */
	public ApiJsonResult<?> queryPostList(Page<TbPostInfo> paramPage, TbPostInfo tbPostInfo);
	/**
	 * 查询当前用户的发布的帖子
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param paramPage
	 * @param tbPostInfo
	 * @return <br>
	 * @CreateDate Apr 24, 2020 8:11:39 PM <br>
	 */
	public String queryUserPostList(Page<TbPostInfo> paramPage, TbPostInfo tbPostInfo);
	
	/**
	 * Description: 保存帖子<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 23, 2020 8:56:34 PM <br>
	 */
	public ApiJsonResult<?> savePost(TbPostInfo tbPostInfo);
	
	/**
	 * Description: 更新帖子<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbPostInfo
	 * @return <br>
	 * @CreateDate Mar 23, 2020 8:58:27 PM <br>
	 */
	public ApiJsonResult<?> updatePost(TbPostInfo tbPostInfo);
	
	/**
	 * Description: 删除帖子<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbPostInfo
	 * @return <br>
	 * @CreateDate Mar 23, 2020 8:58:39 PM <br>
	 */
	public ApiJsonResult<?> delPost(String postId);
	
	/**
	 * Description: 帖子增加阅读量<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 24, 2020 8:48:04 PM <br>
	 */
	public ApiJsonResult<?> addReadCount(String postId);
	
	/**
	 * Description: 查看帖子详情<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 29, 2020 8:46:03 PM <br>
	 */
	public ApiJsonResult<?> readPostInfo(String postId);

}
