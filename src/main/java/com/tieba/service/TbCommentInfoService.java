package com.tieba.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tieba.entity.TbCommentInfo;
import com.tieba.utils.ApiJsonResult;

/**
 * <p>
 * 评论表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbCommentInfoService extends IService<TbCommentInfo> {

	/**
	 * Description: 添加评论<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 21, 2020 9:24:21 PM <br>
	 */
	public ApiJsonResult<?> addComment(TbCommentInfo tbCommentInfo);
	
	/**
	 * Description:根据评论id删除评论 <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param commentId
	 * @return <br>
	 * @CreateDate Mar 21, 2020 9:38:52 PM <br>
	 */
	public ApiJsonResult<?> delComment(String commentId);
	
	/**
	 * Description: 根据帖子id，获取帖子的评论<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 21, 2020 10:49:54 PM <br>
	 */
	public ApiJsonResult<?> getCommentByPostId(String postId, Page<TbCommentInfo> paramPage);
	
	/**
	 * Description: 根据评论id和帖子id，获取评论回复<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 21, 2020 10:49:54 PM <br>
	 */
	public ApiJsonResult<?> getCommentByPostIdAndComId(String postId, String commentId,Page<TbCommentInfo> paramPage);
	/**
	 * 增加回复内容
	 * Description: <br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @param tbCommentInfo
	 * @return <br>
	 * @CreateDate Apr 4, 2020 3:16:11 PM <br>
	 */
	public ApiJsonResult<?> addItemComment(TbCommentInfo tbCommentInfo);
}
