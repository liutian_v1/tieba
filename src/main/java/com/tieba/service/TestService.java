/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tieba.dao.TestDao;

/** 
 * <Description> <br> 
 *  
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 17, 2020 10:23:24 AM <br>
 */
@Service("testService")
public class TestService {
	
	@Autowired
	private TestDao testDao;
	
	public String getTime() {
		return testDao.querySystime();
	}

}
