package com.tieba.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tieba.entity.TbUser;
import com.tieba.utils.ApiJsonResult;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbUserService extends IService<TbUser> {
	/**
	 * 用户信息
	 * @param username
	 * @param passwd
	 * @return
	 */
	public ApiJsonResult<?> queryTbUser(String username, String password);
	/**
	 * 注册用户信息
	 * Description: <br> 
	 * @author lt<br>
	 * @taskId <br>
	 * @param user
	 * @return <br>
	 * @CreateDate 2020年3月21日 下午11:17:42 <br>
	 */
	public ApiJsonResult<?> registerUserInfo(TbUser user);
	/**
	 * 修改用户信息
	 * Description: <br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @param user
	 * @return <br>
	 * @CreateDate Mar 29, 2020 4:06:51 PM <br>
	 */
	public ApiJsonResult<?> updateUserInfo(TbUser user);
	/**
	 * 查询需要添加的用户清单
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param user
	 * @return <br>
	 * @CreateDate Apr 21, 2020 9:54:36 PM <br>
	 */
	public ApiJsonResult<?> queryAllUser(TbUser user);
	
	/**
	 * Description: <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param sysDictPage
	 * @param sysDictType
	 * @return <br>
	 * @CreateDate Apr 1, 2020 11:25:27 PM <br>
	 */ 
	public ApiJsonResult<?> queryList(Page<TbUser> userPage, TbUser user);
}
