package com.tieba.service;

import com.tieba.entity.TbGroup;
import com.tieba.entity.TbGroupMessage;
import com.tieba.utils.ApiJsonResult;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论状态 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbGroupService extends IService<TbGroup> {
	
	
	/**
	 * Description: 创建群组<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroup
	 * @return <br>
	 * @CreateDate Mar 23, 2020 9:06:42 PM <br>
	 */
	public ApiJsonResult<?> createGroup(TbGroup tbGroup);
	
	/**
	 * Description: 删除分组<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroup
	 * @return <br>
	 * @CreateDate Mar 24, 2020 9:30:06 PM <br>
	 */
	public ApiJsonResult<?> delGroup(String groupId, String userId);
	
	/**
	 * Description: 创建群组并加入群组<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroup
	 * @return <br>
	 * @CreateDate Mar 23, 2020 9:06:42 PM <br>
	 */
	public ApiJsonResult<?> createGroupAndUserInfo(TbGroup tbGroup,String parm);
	/**
	 * 查询群组列表
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param pageParam
	 * @return <br>
	 * @CreateDate Apr 22, 2020 9:35:23 PM <br>
	 */
	public ApiJsonResult<?> selectGroupInfo(Page<TbGroup> pageParam,TbGroup tbGroup);

}
