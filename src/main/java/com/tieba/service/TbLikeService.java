package com.tieba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tieba.entity.TbLike;
import com.tieba.utils.ApiJsonResult;

/**
 * <p>
 * 点赞表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbLikeService extends IService<TbLike> {

	/**
	 * Description: <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br> <br>
	 * @CreateDate Mar 19, 2020 3:44:40 PM <br>
	 */ 
	public ApiJsonResult<?> like(String userId, String type, String postOrComtId);

	/**
	 * Description:取消点赞 <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param userId
	 * @param postOrComtId
	 * @return <br>
	 * @throws Exception 
	 * @CreateDate Mar 19, 2020 4:16:52 PM <br>
	 */
	public ApiJsonResult<?> cancelLike(String userId, String postOrComtId);
}
