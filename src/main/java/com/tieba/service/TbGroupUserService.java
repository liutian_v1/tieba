package com.tieba.service;

import com.tieba.entity.TbGroupUser;
import com.tieba.utils.ApiJsonResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 群人员表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbGroupUserService extends IService<TbGroupUser> {
	
	/**
	 * Description: 用户加入群组<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroupUser
	 * @return <br>
	 * @CreateDate Mar 23, 2020 9:22:52 PM <br>
	 */
	public ApiJsonResult<?> addGroup(TbGroupUser tbGroupUser);
	
	/**
	 * Description: 用户退出群组<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroupUser
	 * @return <br>
	 * @CreateDate Mar 23, 2020 9:22:52 PM <br>
	 */
	public ApiJsonResult<?> delGroup(String userId, String groupId);
	
	/**
	 * Description: 修改用户的类型，<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param userId
	 * @param groupId
	 * @return <br>
	 * @CreateDate Mar 23, 2020 10:06:29 PM <br>
	 */
	public ApiJsonResult<?> updateGroupUserType(String groupUserId, String userId, String groupId, int type);
	/**
	 * 根据用户 查询个人消息列表
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Apr 15, 2020 9:23:17 PM <br>
	 */
	public ApiJsonResult<?> queryGroupUserById(String userId);
	/**
	 * 查詢當前群主信息
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param groupId
	 * @return <br>
	 * @CreateDate Apr 22, 2020 10:40:34 PM <br>
	 */
	public ApiJsonResult<?> queryGroupById(String groupId); 

}
