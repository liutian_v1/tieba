package com.tieba.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.aspect.annotation.EnableAutoResult;
import com.tieba.aspect.annotation.LogInfo;
import com.tieba.common.utils.TiebaEnum;
import com.tieba.common.utils.UserContext;
import com.tieba.dao.TbCommentInfoMapper;
import com.tieba.entity.TbCommentInfo;
import com.tieba.entity.TbUser;
import com.tieba.service.TbCommentInfoService;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 评论表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Service
@LogInfo
@EnableAutoResult
public class TbCommentInfoServiceImpl extends ServiceImpl<TbCommentInfoMapper, TbCommentInfo>
		implements TbCommentInfoService {

	@Autowired
	private TbUserService tbUserService;

	/**
	 * Description: 添加评论<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 21, 2020 9:24:34 PM <br>
	 */
	@Override
	public ApiJsonResult<?> addComment(TbCommentInfo tbCommentInfo) {
		// 评论状态为有效
		tbCommentInfo.setCommentStatus(1);
		int insert = baseMapper.insert(tbCommentInfo);
		return R.success(insert, "添加成功！");
	}

	/**
	 * Description:根据评论id删除评论 <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param commentId
	 * @return <br>
	 * @CreateDate Mar 21, 2020 9:38:52 PM <br>
	 */
	public ApiJsonResult<?> delComment(String commentId) {
		UpdateWrapper<TbCommentInfo> updateWrapper = new UpdateWrapper<>();
		updateWrapper.in("comment_id", commentId);
		TbCommentInfo tbCommentInfo = new TbCommentInfo();
		// 设置为不可见
		tbCommentInfo.setCommentStatus(TiebaEnum.HIDE.getType());
		int update = baseMapper.update(tbCommentInfo, updateWrapper);
		if (update == 1) {
			// 主评论删除后，所有楼层也被删除
			updateWrapper = new UpdateWrapper<>();
			updateWrapper.in("comment_parent_id", commentId);
			update = baseMapper.update(tbCommentInfo, updateWrapper);
			return new ApiJsonResult<>().setMsg("删除成功");
		}
		return R.wrong("删除失败！");
	}

	/**
	 * Description: 根据帖子id，获取帖子的评论<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 21, 2020 10:49:54 PM <br>
	 */
	public ApiJsonResult<?> getCommentByPostId(String postId, Page<TbCommentInfo> paramPage) {
		QueryWrapper<TbCommentInfo> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("post_id", postId);
		queryWrapper.isNull("comment_parent_id");
		queryWrapper.eq("comment_status", TiebaEnum.SHOW.getType()); // 1为可见
		queryWrapper.orderByAsc("comment_time");
		IPage<TbCommentInfo> selectPage = baseMapper.selectPage(paramPage, queryWrapper);
		// 循环获取 用户的头像和昵称的相关信息
		selectPage.getRecords().forEach(items -> {
			String userId = items.getUserId();
			// 查询用户信息
			TbUser userInfo = tbUserService.getById(userId);
			String userName = "";
			String headImageUrl = "";
			if (userInfo != null) {
				headImageUrl = userInfo.getHeadImageUrl();
				userName = userInfo.getUserName();
			}
			items.setHeadImageUrl(headImageUrl);
			items.setUserName(userName);
		});
		return R.success(selectPage, "查询成功");
	}

	/**
	 * Description: 根据评论id和帖子id，获取评论回复<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 21, 2020 10:49:54 PM <br>
	 */
	public ApiJsonResult<?> getCommentByPostIdAndComId(String postId, String commentId, Page<TbCommentInfo> paramPage) {
		QueryWrapper<TbCommentInfo> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("post_id", postId);
		queryWrapper.eq("comment_parent_id", commentId);
		queryWrapper.eq("comment_status", TiebaEnum.SHOW.getType()); // 1为可见
		queryWrapper.orderByAsc("comment_time");
		IPage<TbCommentInfo> selectPage = baseMapper.selectPage(paramPage, queryWrapper);
		// 循环获取 用户的头像和昵称的相关信息
		selectPage.getRecords().forEach(items -> {
			String userId = items.getUserId();
			String toUserId = items.getToUserId();
			// 查询用户信息
			TbUser userInfo = tbUserService.getById(userId);
			String userName = "";
			String headImageUrl = "";
			String toUserName = "";
			String toHeadImageUrl = "";
			if (userInfo != null) {
				headImageUrl = userInfo.getHeadImageUrl();
				userName = userInfo.getUserName();
			}
			// 查询被回复用户信息
			TbUser toUserInfo = tbUserService.getById(toUserId);
			if (toUserInfo != null) {
				toHeadImageUrl = toUserInfo.getHeadImageUrl();
				toUserName = toUserInfo.getUserName();
			}
			items.setToHeadImageUrl(toHeadImageUrl);
			items.setToUserName(toUserName);
			items.setHeadImageUrl(headImageUrl);
			items.setUserName(userName);
		});
		return R.success(selectPage, "查询成功");
	}

	@Override
	public ApiJsonResult<?> addItemComment(TbCommentInfo tbCommentInfo) {
		// 评论状态为有效
		tbCommentInfo.setCommentStatus(1);
		// 根据上一几点获得回复内人的相关信息
		QueryWrapper<TbCommentInfo> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("comment_id", tbCommentInfo.getCommentParentId());
		TbCommentInfo selectOne = baseMapper.selectOne(queryWrapper);
		if (null != selectOne) {
			//根据用户Id 查询用户信
			TbUser tbUser=tbUserService.getById(selectOne.getUserId());
			tbCommentInfo.setUserName(tbUser.getUserName());
			tbCommentInfo.setToUserId(selectOne.getUserId());
		}
		int insert = baseMapper.insert(tbCommentInfo);
		if(insert==1) {//成功
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime date3 = LocalDateTime.now();
			tbCommentInfo.setCommentTime(date3.format(dtf));
		}
		return R.success(tbCommentInfo, "添加成功！");
	}

}
