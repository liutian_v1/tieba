package com.tieba.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.aspect.annotation.EnableAutoResult;
import com.tieba.aspect.annotation.LogInfo;
import com.tieba.common.utils.TiebaEnum;
import com.tieba.dao.TbGroupUserMapper;
import com.tieba.entity.TbGroup;
import com.tieba.entity.TbGroupMessage;
import com.tieba.entity.TbGroupUser;
import com.tieba.entity.TbUser;
import com.tieba.service.TbGroupMessageService;
import com.tieba.service.TbGroupService;
import com.tieba.service.TbGroupUserService;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 群人员表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Service
@EnableAutoResult
@LogInfo
public class TbGroupUserServiceImpl extends ServiceImpl<TbGroupUserMapper, TbGroupUser> implements TbGroupUserService {
	
	@Autowired
	private TbGroupService tbGroupService;
	@Autowired
	private TbGroupMessageService tbGroupMessageService;
	@Autowired
	private TbUserService tbUserService;
	/**
	 * Description: 用户加入群组<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroupUser
	 * @return <br>
	 * @CreateDate Mar 23, 2020 9:22:52 PM <br>
	 */
	public ApiJsonResult<?> addGroup(TbGroupUser tbGroupUser) {
		QueryWrapper<TbGroupUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("group_id", tbGroupUser.getGroupId());
		queryWrapper.eq("user_id", tbGroupUser.getUserId());
		TbGroupUser selectOne = baseMapper.selectOne(queryWrapper);
		if (selectOne != null) {
			return R.fail("禁止重复加群~");
		}
		// 加入群组都是用户类型
		tbGroupUser.setGroupUserType(TiebaEnum.GROUPUSERTYPE_USER.getType());
		int insert = baseMapper.insert(tbGroupUser);
		return R.success(insert, "加群成功");
	}

	/**
	 * Description: 用户退出群组<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroupUser
	 * @return <br>
	 * @CreateDate Mar 23, 2020 9:22:52 PM <br>
	 */
	public ApiJsonResult<?> delGroup(String userId, String groupId) {
		QueryWrapper<TbGroupUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("group_id", groupId);
		queryWrapper.eq("user_id", userId);
		TbGroupUser selectOne = baseMapper.selectOne(queryWrapper);
		if (selectOne != null) {
			return R.fail("用户不在此群中");
		}
		UpdateWrapper<TbGroupUser> wrapper = new UpdateWrapper<>();
		wrapper.eq("user_id", userId);
		wrapper.eq("group_id", groupId);
		int delete = baseMapper.delete(wrapper);
		return R.success(delete, "退出成功！");
	}

	/**
	 * Description: 修改用户的类型，<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param userId
	 * @param groupId
	 * @return <br>
	 * @CreateDate Mar 23, 2020 10:06:29 PM <br>
	 */
	public ApiJsonResult<?> updateGroupUserType(String groupUserId, String userId, String groupId, int type) {
		QueryWrapper<TbGroupUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("group_id", groupId);
		queryWrapper.eq("user_id", groupUserId);
		queryWrapper.eq("group_user_type", TiebaEnum.GROUPUSERTYPE_MAIN.getType());
		TbGroupUser selectOne = baseMapper.selectOne(queryWrapper);
		if (selectOne != null) {
			// 此操作可以将普通用户修改为管理员或者将管理员修改为用户
			if (type == TiebaEnum.GROUPUSERTYPE_USER.getType() || type == TiebaEnum.GROUPUSERTYPE_ADMIN.getType()) {
				UpdateWrapper<TbGroupUser> wrapper = new UpdateWrapper<>();
				wrapper.eq("group_id", groupId);
				wrapper.eq("user_id", userId);
				TbGroupUser tbGroupUser = new TbGroupUser();
				tbGroupUser.setGroupUserType(type);
				baseMapper.update(tbGroupUser, wrapper);
				return R.success("修改成功，用户类型成功更新");
			}
		} else {
			return R.fail("不是群主，无法修改用户的群类型");
		}
		return R.fail("修改失败，类型不符合");
	}
	/**
	 * 查询用户群列表
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param userId
	 * @return <br>
	 * @CreateDate Apr 15, 2020 10:08:38 PM <br>
	 */
	@Override
	public ApiJsonResult<?> queryGroupUserById(String userId) {
		QueryWrapper<TbGroupUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", userId);
		List<TbGroupUser> seleclist = baseMapper.selectList(queryWrapper);
		seleclist.forEach(item ->{
			//查询群相关信息
			QueryWrapper<TbGroup> queryGroup= new QueryWrapper<>();
			queryGroup.eq("group_id", item.getGroupId());
			TbGroup groupOne= tbGroupService.getById(item.getGroupId());
			String groupName=groupOne.getGroupName();
			item.setGroupName(groupName);
			//取最近的一条消息内容,并加载到用户群表里面 以便前台显示
			TbGroupMessage queryCurrentMessage = tbGroupMessageService.queryCurrentMessage(item.getGroupId());
			if(null!=queryCurrentMessage) {
				item.setMessage(queryCurrentMessage.getMessage());
			}
		});
		return R.success(seleclist, "查询成功");
	}

	@Override
	public ApiJsonResult<?> queryGroupById(String groupId) {
		QueryWrapper<TbGroup> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("group_id", groupId);
		TbGroup tbGroup=tbGroupService.getById(groupId);
		//根據群組 好的當前的群組人員
		QueryWrapper<TbGroupUser> queryGroupUser = new QueryWrapper<>();
		queryGroupUser.eq("group_id", groupId);
		List<TbGroupUser> seleclist = baseMapper.selectList(queryGroupUser);
		JSONArray arrary=new JSONArray();
		String [] addValu=new String[seleclist.size()];
		for (int j = 0; j < seleclist.size(); j++) {
			addValu[j]=seleclist.get(j).getUserId();
		}
		JSONObject obj=new JSONObject();
		obj.put("tbGroup", JSON.toJSON((tbGroup)));
		obj.put("data1", addValu);
		obj.put("data2", tbUserService.queryAllUser(null));
		return R.success(obj, "成功");
	}

}
