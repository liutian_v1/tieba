package com.tieba.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.aspect.annotation.EnableAutoResult;
import com.tieba.aspect.annotation.LogInfo;
import com.tieba.dao.TbLikeMapper;
import com.tieba.entity.TbLike;
import com.tieba.service.TbLikeService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 点赞表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Service
@LogInfo
@EnableAutoResult
public class TbLikeServiceImpl extends ServiceImpl<TbLikeMapper, TbLike> implements TbLikeService {

	/**
	 * Description: 点赞 <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 19, 2020 3:45:14 PM <br>
	 */
	@Override
	public ApiJsonResult<?> like(String userId, String type, String postOrComtId) {
		TbLike tbLike = new TbLike();
		tbLike.setLikeType(Integer.parseInt(type));
		tbLike.setUserId(userId);
		tbLike.setPostOrComtId(postOrComtId);
		int insert = this.baseMapper.insert(tbLike);
		return R.success(insert, "点赞成功");
	}

	/**
	 * Description: 取消点赞 <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param userId
	 * @param postOrComtId
	 * @return <br>
	 * @throws Exception
	 * @CreateDate Mar 19, 2020 4:17:04 PM <br>
	 */
	@Override
	public ApiJsonResult<?> cancelLike(String userId, String postOrComtId) {
		TbLike tbLike = new TbLike();
		tbLike.setUserId(userId);
		tbLike.setPostOrComtId(postOrComtId);
		UpdateWrapper<TbLike> wrapper = new UpdateWrapper<>();
		wrapper.setEntity(tbLike);
		int delete = this.baseMapper.delete(wrapper);
		return R.success(delete, "取消点赞成功");
	}
}
