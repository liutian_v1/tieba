package com.tieba.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.aspect.annotation.EnableAutoResult;
import com.tieba.aspect.annotation.LogInfo;
import com.tieba.dao.TbGroupMessageMapper;
import com.tieba.entity.TbGroupMessage;
import com.tieba.entity.TbUser;
import com.tieba.service.TbGroupMessageService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 群消息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Service
@LogInfo
@EnableAutoResult
public class TbGroupMessageServiceImpl extends ServiceImpl<TbGroupMessageMapper, TbGroupMessage> implements TbGroupMessageService {
	@Autowired
	private TbUserServiceImpl tbUserServiceImpl;
	/**
	 * Description: 发送一条消息<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:09:56 PM <br>
	 */
	public ApiJsonResult<?> sendMessage(TbGroupMessage tbGroupMessage) {
		int insert = baseMapper.insert(tbGroupMessage);
		return R.success(insert, "发送成功");
	}
	
	/**
	 * Description: 删除一条消息 <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param messageId
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:12:02 PM <br>
	 */
	public ApiJsonResult<?> delMessage(String messageId) {
		int deleteById = baseMapper.deleteById(messageId);
		return R.success(deleteById, "删除成功");
	}
	
	/**
	 * Description: 拉取消息列表<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param pageParam
	 * @param tbGroupMessage
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:13:57 PM <br>
	 */
	public ApiJsonResult<?> queryMessageList(Page<TbGroupMessage> pageParam, TbGroupMessage tbGroupMessage) {
		String groupId = tbGroupMessage.getGroupId();
		if(StringUtils.isNotBlank(groupId)) {
			QueryWrapper<TbGroupMessage> queryWrapper = new QueryWrapper<>();
			queryWrapper.setEntity(tbGroupMessage);
			IPage<TbGroupMessage> selectPage = baseMapper.selectPage(pageParam, queryWrapper);
			selectPage.getRecords().forEach(item->{//补充用户信息
				TbUser byId = tbUserServiceImpl.getById(item.getUserId());
				item.setUserName(byId.getUserName());
			});
			return R.success(selectPage, "查询成功");
		} else {
			return R.fail("群id不能为空");
		}
	}

	@Override
	public TbGroupMessage queryCurrentMessage(String groupId) {
		QueryWrapper<TbGroupMessage> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("group_id", groupId);
		queryWrapper.orderByAsc("create_time");
		List<TbGroupMessage> tbGroupInfo=baseMapper.selectList(queryWrapper);
		if(null!=tbGroupInfo && tbGroupInfo.size()>0) {
			return tbGroupInfo.get(0);
		}
		return null;
	}
}
