package com.tieba.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.aspect.annotation.LogInfo;
import com.tieba.common.utils.TiebaEnum;
import com.tieba.common.utils.UserContext;
import com.tieba.dao.TbGroupMapper;
import com.tieba.entity.SysDictType;
import com.tieba.entity.TbGroup;
import com.tieba.entity.TbGroupMessage;
import com.tieba.entity.TbGroupUser;
import com.tieba.entity.TbUser;
import com.tieba.service.TbGroupService;
import com.tieba.service.TbGroupUserService;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 评论状态 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Service
@LogInfo
public class TbGroupServiceImpl extends ServiceImpl<TbGroupMapper, TbGroup> implements TbGroupService {

	private static final Logger logger = LoggerFactory.getLogger(TbGroupServiceImpl.class);

	@Autowired
	private TbGroupUserService groupUserService;

	@Autowired
	private TbUserService userService;

	/**
	 * Description: 创建群组<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroup
	 * @return <br>
	 * @CreateDate Mar 23, 2020 9:06:42 PM <br>
	 */
	@Transactional(rollbackFor = Exception.class)
	public ApiJsonResult<?> createGroup(TbGroup tbGroup) {
		String groupName = tbGroup.getGroupName();
		QueryWrapper<TbGroup> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("group_name", groupName);
		queryWrapper.eq("group_status", TiebaEnum.SHOW.getType());
		TbGroup selectOne = baseMapper.selectOne(queryWrapper);
		if (selectOne != null) {
			return R.fail("创建失败，已经有相同名称的群组");
		}
		int insert = baseMapper.insert(tbGroup);
		if (insert == 1) {
			TbGroupUser tbGroupUserb = new TbGroupUser();
			tbGroupUserb.setGroupId(tbGroup.getGroupId());
			tbGroupUserb.setGroupUserId(tbGroup.getUserId());
			tbGroupUserb.setGroupUserType(TiebaEnum.GROUPUSERTYPE_MAIN.getType());
			boolean save = groupUserService.save(tbGroupUserb);
			logger.info("用户加群保存结果：{}", save);
		}
		return R.success(insert, "创建成功");
	}

	/**
	 * Description: 删除群组<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbGroup
	 * @param userId
	 * @return <br>
	 * @CreateDate Mar 24, 2020 9:32:19 PM <br>
	 */ 
	@Override
	public ApiJsonResult<?> delGroup(String groupId, String userId) {
		if (StringUtils.isBlank(userId)) {
			return R.fail("用户id不能为空");
		}
		if (StringUtils.isBlank(groupId)) {
			return R.fail("群组id不能为空");
		}
		TbUser tbUser = userService.getById(userId);
		if (tbUser == null) {
			return R.fail("群组删除失败，");
		}
		// 必须是群拥有者 或者是贴吧管理员 才可以解散群
		if (tbUser.getUserType() == TiebaEnum.USER_TYPE_ADMIN.getType()) {
			int deleteById = baseMapper.deleteById(groupId);
			return R.success(deleteById, "删除成功,贴吧管理员解散");
		}
		
		QueryWrapper<TbGroupUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("group_id", groupId);
		queryWrapper.eq("user_id", userId);
		TbGroupUser groupUser = groupUserService.getOne(queryWrapper);
		if (groupUser != null) {
			if (groupUser.getGroupUserType()==TiebaEnum.GROUPUSERTYPE_MAIN.getType()) {
				int deleteById = baseMapper.deleteById(groupId);
				return R.success(deleteById, "删除成功,群拥有者解散");
			}
		}
		return R.fail("删除失败, 用户没有解散此群的权限");
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ApiJsonResult<?> createGroupAndUserInfo(TbGroup tbGroup, String parm) {
		String groupName = tbGroup.getGroupName();
		QueryWrapper<TbGroup> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("group_name", groupName);
		queryWrapper.eq("group_status", TiebaEnum.SHOW.getType());
		TbGroup selectOne = baseMapper.selectOne(queryWrapper);
		if (selectOne != null) {
			return R.fail("创建失败，已经有相同名称的群组");
		}
		TbUser tbUser=UserContext.getCurreentUser();
		tbGroup.setUserId(tbUser.getUserId());
		tbGroup.setGroupStatus(1);
		tbGroup.setGroupType(1);
		int insert = baseMapper.insert(tbGroup);
		if(insert==1){
			JSONArray arrary=JSONArray.parseArray(parm);
			for (int i = 0; i < arrary.size(); i++) {
				JSONObject item=arrary.getJSONObject(i);
				TbGroupUser tbGroupUserb = new TbGroupUser();
				tbGroupUserb.setGroupId(tbGroup.getGroupId());
				tbGroupUserb.setUserId(item.getString("value"));
				tbGroupUserb.setGroupUserType(TiebaEnum.GROUPUSERTYPE_MAIN.getType());
				boolean save = groupUserService.save(tbGroupUserb);
				logger.info("用户加群保存结果：{}", save);
			}
		}
		return R.success();
	}

	@Override
	public ApiJsonResult<?> selectGroupInfo(Page<TbGroup> pageParam,TbGroup tbGroup) {
		QueryWrapper<TbGroup> queryWrapper = new QueryWrapper<>();
		queryWrapper.setEntity(tbGroup);
		IPage<TbGroup> selectPage = baseMapper.selectPage(pageParam, queryWrapper);
		selectPage.getRecords().forEach(item->{//补充用户信息
			TbUser byId = userService.getById(item.getUserId());
			item.setUserName(byId.getUserName());
		});
		List<TbGroup> selectList =selectPage.getRecords();
		int total = (int) selectPage.getTotal();
		return R.success(selectList, "查询成功", total);
	}

}
