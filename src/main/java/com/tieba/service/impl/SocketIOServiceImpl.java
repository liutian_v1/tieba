/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.tieba.entity.TbGroupUser;
import com.tieba.service.TbGroupUserService;

/**
 * <Description> <br>
 * 
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Apr 13, 2020 8:26:55 PM <br>
 */
@Component
public class SocketIOServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(SocketIOServiceImpl.class);

	@Autowired
	private SocketIOServer socketIoServer;
	
	@Autowired
	private TbGroupUserService tbGroupUserService;

	// 用来存已连接的客户端
	private static Map<String, SocketIOClient> clientMap = new ConcurrentHashMap<>();

	/**
	 * Spring IoC容器创建之后，在加载SocketIOServiceImpl Bean之后启动
	 * 
	 * @throws Exception
	 */
	@PostConstruct
	private void autoStartup() throws Exception {
		start();
	}

	/**
	 * Spring IoC容器在销毁SocketIOServiceImpl Bean之前关闭,避免重启项目服务端口占用问题
	 * 
	 * @throws Exception
	 */
	@PreDestroy
	private void autoStop() throws Exception {
		stop();
	}

	/**
	 * Description: <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @throws Exception <br>
	 * @CreateDate Apr 13, 2020 8:44:32 PM <br>
	 */
	public void start() throws Exception {
		// 监听客户端连接
		socketIoServer.addConnectListener(client -> {
			String userId = getParamsByClient(client);
			if (userId != null) {
				logger.info(client.getSessionId() + " connect");
				clientMap.put(client.getSessionId().toString(), client);
			}
		});
		// 监听客户端断开连接
		socketIoServer.addDisconnectListener(client -> {
			String userId = getParamsByClient(client);
			if (userId != null) {
				logger.info(client.getSessionId() + " Disconnect");
				clientMap.remove(client.getSessionId().toString());
				client.disconnect();
			}
		});
		socketIoServer.start();
	}
	
	/**
	 * Description: 加群<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param socketIOClient
	 * @param ackRequest
	 * @param msg            <br>
	 * @throws JSONException 
	 * @CreateDate Apr 14, 2020 10:40:31 AM <br>
	 */
	@OnEvent("joinRoomList")
	public void joinRoomList(SocketIOClient socketIOClient, JSONObject param) {
		String userId = param.getString("userId");
		QueryWrapper<TbGroupUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", userId);
		// 查用户所有的群， 并且加入房间
		List<TbGroupUser> list = tbGroupUserService.list(queryWrapper);
		Set<String> allRooms = socketIOClient.getAllRooms();
		String groupPix = "group_";
		allRooms.forEach(room -> {
			if (room.startsWith(groupPix)) {
				socketIOClient.leaveRoom(room);
			}
		});
		list.forEach(items -> {
			String groupId = items.getGroupId();
			String room = groupPix+ groupId;
			socketIOClient.joinRoom(room);
		});
		socketIOClient.sendEvent("joinRoomSuc", list);
	} 
	
	/**
	 * Description: <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 *         <br>
	 * @CreateDate Apr 13, 2020 8:44:32 PM <br>
	 */
	public void stop() {
		if (socketIoServer != null) {
			socketIoServer.stop();
			socketIoServer = null;
		}
	}


	/**
	 * 此方法为获取client连接中的参数，可根据需求更改
	 * 
	 * @param client
	 * @return
	 */
	private String getParamsByClient(SocketIOClient client) {
		// 从请求的连接中拿出参数（这里的userId必须是唯一标识）
		Map<String, List<String>> params = client.getHandshakeData().getUrlParams();
		List<String> list = params.get("userId");
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
	
	public Map<String, SocketIOClient> getClientMap() {
		return clientMap;
	}
}
