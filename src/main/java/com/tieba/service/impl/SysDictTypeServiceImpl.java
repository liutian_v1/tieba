package com.tieba.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.aspect.annotation.EnableAutoResult;
import com.tieba.dao.SysDictTypeMapper;
import com.tieba.entity.SysDictType;
import com.tieba.service.SysDictTypeService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Service
@EnableAutoResult
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {

	
	/**
	 * Description: 根据类型获取字典信息<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param type
	 * @return <br>
	 * @CreateDate Mar 22, 2020 10:39:58 PM <br>
	 */
	public ApiJsonResult<?> getDictByType(String type) {
		QueryWrapper<SysDictType> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("dict_type", type);
		queryWrapper.eq("status", 1);
		List<SysDictType> selectList = baseMapper.selectList(queryWrapper);
		return R.success(selectList, "查询成功");
	}
	
	/**
	 * Description: 根据类型和键值信息查询字典信息<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param type
	 * @param value
	 * @return <br>
	 * @CreateDate Mar 22, 2020 10:44:53 PM <br>
	 */
	public ApiJsonResult<?> getDictByType(String type, String value) {
		QueryWrapper<SysDictType> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("dict_type", type);
		queryWrapper.eq("status", 1);
		queryWrapper.eq("dict_value", value);
		List<SysDictType> selectList = baseMapper.selectList(queryWrapper);
		return R.success(selectList, "查询成功");
	}

	/**
	 * Description: <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param sysDictPage
	 * @param sysDictType
	 * @return <br>
	 * @CreateDate Apr 1, 2020 11:25:42 PM <br>
	 */ 
	@Override
	public ApiJsonResult<?> queryList(Page<SysDictType> sysDictPage, SysDictType sysDictType) {
		QueryWrapper<SysDictType>  queryWrapper = new QueryWrapper<>();
		queryWrapper.setEntity(sysDictType);
		queryWrapper.orderByAsc("dict_type","dict_value");
		IPage<SysDictType> selectPage = baseMapper.selectPage(sysDictPage, queryWrapper);
		List<SysDictType> selectList =selectPage.getRecords();
		int total = (int) selectPage.getTotal();
		return R.success(selectList, "查询成功", total);
	}
}
