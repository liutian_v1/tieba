package com.tieba.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.aspect.annotation.EnableAutoResult;
import com.tieba.aspect.annotation.LogInfo;
import com.tieba.common.utils.UserContext;
import com.tieba.dao.TbUserMapper;
import com.tieba.entity.TbUser;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Service
@EnableAutoResult
@LogInfo
public class TbUserServiceImpl extends ServiceImpl<TbUserMapper, TbUser> implements TbUserService {
	/**
	 * 日志组件
	 */
	private static final Logger logger = LoggerFactory.getLogger(TbUserServiceImpl.class);

	/**
	 * Description: 用户信息 <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 19, 2020 3:45:14 PM <br>
	 */
	@Override
	public ApiJsonResult<?> queryTbUser(String username, String password) {
		ApiJsonResult<?> resultApi = new ApiJsonResult<>();
		QueryWrapper<TbUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("login_name", username);
		queryWrapper.eq("password", password);
		TbUser selectOne = baseMapper.selectOne(queryWrapper);
		if (null == selectOne) {
			logger.info(username + "用户登陆失败：");
			resultApi.setMsg("用户名或者密码错误");
			resultApi.setSuccess(false);
			return resultApi;
		}
		resultApi = R.success(selectOne, "登录成功");
		// 用户信息写入session
		UserContext.putCurrebtUser(selectOne);
	return resultApi;
	}

	/**
	 * Description: <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param user
	 * @return <br>
	 * @CreateDate 2020年3月21日 下午11:18:27 <br>
	 */ 
	@Override
	public ApiJsonResult<?> registerUserInfo(TbUser user) {
		ApiJsonResult<?> resultApi = new ApiJsonResult<>();
		// TODO Auto-generated method stub
		//验证用户是否存在
		QueryWrapper<TbUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("login_name", user.getLoginName());
		queryWrapper.eq("password", user.getPassword());
		TbUser selectOne = baseMapper.selectOne(queryWrapper);
		if(null!=selectOne) {
			return R.wrong("用户已存在");
		}
		int result=this.baseMapper.insert(user);
		if(1==result) {
			return R.success("用户注册成功");
		}
	    return R.wrong("注册失败");
	}

	@Override
	public ApiJsonResult<?> updateUserInfo(TbUser user) {
//		QueryWrapper<TbUser> queryWrapper = new QueryWrapper<>();
//		queryWrapper.eq("user_name", user.getUserName());
//		queryWrapper.eq("password", user.getPassword());
//		queryWrapper.eq("sex", user.getSex());
//		queryWrapper.eq("head_image_url", user.getHeadImageUrl());
	//	queryWrapper.eq("email", user.getEmail());
		//执行数据库操作
		int result=baseMapper.updateById(user);
		logger.info("更新用户信息："+user.getUserId()+"状态："+(result==1?"成功":"失败"));
		if(result==1) {
			UserContext.putCurrebtUser(user);
			return R.success();
		}
		return R.wrong("操作失败，系统异常");
	}

	@Override
	public ApiJsonResult<?> queryAllUser(TbUser user) {
		QueryWrapper<TbUser> queryWrapper = new QueryWrapper<>();
		List<TbUser> selectList = baseMapper.selectList(queryWrapper);
		JSONArray arrary=new JSONArray();
		selectList.forEach(item->{
			JSONObject obj=new JSONObject();
			obj.put("value", item.getUserId());
			obj.put("title", item.getLoginName());
			arrary.add(obj);
		});
		return R.success(arrary,"成功");
	}
	
	/**
	 * Description: <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param sysDictPage
	 * @param sysDictType
	 * @return <br>
	 * @CreateDate Apr 1, 2020 11:25:27 PM <br>
	 */ 
	public ApiJsonResult<?> queryList(Page<TbUser> userPage, TbUser user) {
		QueryWrapper<TbUser>  queryWrapper = new QueryWrapper<>();
		queryWrapper.setEntity(user);
		queryWrapper.orderByDesc("create_time");
		IPage<TbUser> selectPage = baseMapper.selectPage(userPage, queryWrapper);
		List<TbUser> selectList =selectPage.getRecords();
		int total = (int) selectPage.getTotal();
		return R.success(selectList, "查询成功", total);
	}

	
}
