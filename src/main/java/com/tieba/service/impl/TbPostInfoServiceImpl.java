package com.tieba.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.aspect.annotation.LogInfo;
import com.tieba.common.utils.TiebaEnum;
import com.tieba.common.utils.UserContext;
import com.tieba.dao.TbPostInfoMapper;
import com.tieba.entity.TbPostInfo;
import com.tieba.entity.TbUser;
import com.tieba.service.TbPostInfoService;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 帖子表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Service
@LogInfo
public class TbPostInfoServiceImpl extends ServiceImpl<TbPostInfoMapper, TbPostInfo> implements TbPostInfoService {

	@Autowired
	private TbUserService tbUserService;

	/**
	 * Description: 查询帖子列表<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param paramPage
	 * @return <br>
	 * @CreateDate Mar 22, 2020 11:09:04 PM <br>
	 */
	public ApiJsonResult<?> queryPostList(Page<TbPostInfo> paramPage, TbPostInfo tbPostInfo) {
		QueryWrapper<TbPostInfo> queryWrapper = new QueryWrapper<>();
		tbPostInfo.setPostStuatus(1);
		queryWrapper.setEntity(tbPostInfo);
		queryWrapper.orderByDesc("create_time");
		IPage<TbPostInfo> selectPage = baseMapper.selectPage(paramPage, queryWrapper);
		// 循环获取 用户的头像和昵称的相关信息
		selectPage.getRecords().forEach(items -> {
			String userId = items.getPostUserId();
			// 查询用户信息
			TbUser userInfo = tbUserService.getById(userId);
			String userName = "";
			String headImageUrl = "";
			if (userInfo != null) {
				headImageUrl = userInfo.getHeadImageUrl();
				userName = userInfo.getUserName();
			}
			items.setHeadImageUrl(headImageUrl);
			items.setUserName(userName);
		});
		return R.success(selectPage, "查询成功");
	}

	/**
	 * Description: 保存帖子<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 23, 2020 8:56:34 PM <br>
	 */
	public ApiJsonResult<?> savePost(TbPostInfo tbPostInfo) {
		baseMapper.insert(tbPostInfo);
		return R.success("发帖成功！");
	}

	/**
	 * Description: 更新帖子<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbPostInfo
	 * @return <br>
	 * @CreateDate Mar 23, 2020 8:58:27 PM <br>
	 */
	public ApiJsonResult<?> updatePost(TbPostInfo tbPostInfo) {
		baseMapper.updateById(tbPostInfo);
		return R.success("修改成功！");
	}

	/**
	 * Description: 删除帖子<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param tbPostInfo
	 * @return <br>
	 * @CreateDate Mar 23, 2020 8:58:39 PM <br>
	 */
	public ApiJsonResult<?> delPost(String postId) {
		TbPostInfo tbPostInfo = new TbPostInfo();
		tbPostInfo.setPostId(postId);
		tbPostInfo.setPostType(TiebaEnum.HIDE.getType());
		baseMapper.updateById(tbPostInfo);
		return R.success("删除成功！");
	}
	
	/**
	 * Description: 帖子增加阅读量<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 24, 2020 8:48:04 PM <br>
	 */
	public ApiJsonResult<?> addReadCount(String postId) {
		int addReadCount = baseMapper.addReadCount(postId);
		return R.success(addReadCount, "添加成功");
	}
	
	/**
	 * Description: 查看帖子详情<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 29, 2020 8:46:03 PM <br>
	 */
	public ApiJsonResult<?> readPostInfo(String postId) {
		TbPostInfo selectById = baseMapper.selectById(postId);
		return R.success(selectById, "查询成功");
	}

	@Override
	public String queryUserPostList(Page<TbPostInfo> paramPage, TbPostInfo tbPostInfo) {
		QueryWrapper<TbPostInfo> queryWrapper = new QueryWrapper<>();
		tbPostInfo.setPostStuatus(1);
		queryWrapper.setEntity(tbPostInfo);
		TbUser tbuser=UserContext.getCurreentUser();
		queryWrapper.eq("post_user_id", tbuser.getUserId());
		queryWrapper.orderByDesc("create_time");
		IPage<TbPostInfo> selectPage = baseMapper.selectPage(paramPage, queryWrapper);
		// 循环获取 用户的头像和昵称的相关信息
		selectPage.getRecords().forEach(items -> {
			String userId = items.getPostUserId();
			// 查询用户信息
			TbUser userInfo = tbUserService.getById(userId);
			String userName = "";
			String headImageUrl = "";
			if (userInfo != null) {
				headImageUrl = userInfo.getHeadImageUrl();
				userName = userInfo.getUserName();
			}
			items.setHeadImageUrl(headImageUrl);
			items.setUserName(userName);
		});
		JSONObject obj=new JSONObject();
		obj.put("code", "0");
		obj.put("total", selectPage.getRecords().size());
		JSONArray array=JSONArray.parseArray(JSON.toJSONString( selectPage.getRecords()));
		obj.put("data", array);
		return obj.toJSONString();
	}

}
