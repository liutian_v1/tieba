package com.tieba.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.tieba.entity.TbUser;
import com.tieba.service.TbUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tieba.dao.TbCollectMapper;
import com.tieba.entity.TbCollect;
import com.tieba.entity.TbPostInfo;
import com.tieba.service.TbCollectService;
import com.tieba.service.TbPostInfoService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 收藏表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-27
 */
@Service
public class TbCollectServiceImpl extends ServiceImpl<TbCollectMapper, TbCollect> implements TbCollectService {

    @Autowired
    private TbPostInfoService tbPostInfoService;

    @Autowired
    private TbUserService tbUserService;

    /**
     * Description: 添加收藏 <br>
     *
     * @param tbCollect
     * @return <br>
     * @author lt<br>
     * @taskId <br>
     * @CreateDate Mar 27, 2020 10:36:15 PM <br>
     */
    @Override
    public ApiJsonResult<?> addCollect(TbCollect tbCollect) {
        if (StringUtils.isBlank(tbCollect.getUserId())) {
            return R.fail("用户id不能为空");
        } else if (StringUtils.isBlank(tbCollect.getPostId())) {
            return R.fail("帖子id不能为空");
        }
        // 先删除在增加，防止重复收藏
        UpdateWrapper<TbCollect> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("post_id", tbCollect.getPostId());
		updateWrapper.eq("user_id", tbCollect.getUserId());
        baseMapper.delete(updateWrapper);

        // 保存收藏
        int insert = baseMapper.insert(tbCollect);
        return R.success(insert, "收藏成功");
    }

    /**
     * Description: 取消收藏<br>
     *
     * @param collectId
     * @return <br>
     * @author lt<br>
     * @taskId <br>
     * @CreateDate Mar 27, 2020 10:36:15 PM <br>
     */
    @Override
    public ApiJsonResult<?> cancleCollect(String collectId) {
        int deleteById = baseMapper.deleteById(collectId);
        if (deleteById == 1) {
            return R.success("取消成功");
        }
        return R.fail("取消失败");
    }

    /**
     * Description: 收藏列表查询<br>
     *
     * @param pageInfo
     * @param tbCollect
     * @return <br>
     * @author lt<br>
     * @taskId <br>
     * @CreateDate Apr 7, 2020 10:28:23 PM <br>
     */
    @Override
    public ApiJsonResult<?> queryCollectList(Page<TbCollect> pageInfo, TbCollect tbCollect) {
        QueryWrapper<TbCollect> queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(tbCollect);
        queryWrapper.orderByDesc("create_time");
        IPage<TbCollect> selectPage = baseMapper.selectPage(pageInfo, queryWrapper);
        // 循环获取帖子的名字信息
        selectPage.getRecords().forEach(items -> {
            String postId = items.getPostId();
            QueryWrapper<TbPostInfo> postWrapper = new QueryWrapper<>();
            postWrapper.eq("post_id", postId);
            postWrapper.eq("post_stuatus", 1);
            TbPostInfo tbPostInfo = tbPostInfoService.getOne(postWrapper);
            if (tbPostInfo != null) {
                // 查询用户信息
                TbUser userInfo = tbUserService.getById(tbPostInfo.getPostUserId());
                String userName = "";
                String headImageUrl = "";
                if (userInfo != null) {
                    headImageUrl = userInfo.getHeadImageUrl();
                    userName = userInfo.getUserName();
                }
                tbPostInfo.setHeadImageUrl(headImageUrl);
                tbPostInfo.setUserName(userName);
                items.setTbPostInfo(tbPostInfo);
            }
        });
        return R.success(selectPage, "查询成功");
    }

}
