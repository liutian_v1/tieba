package com.tieba.service;

import com.tieba.entity.TbGroupMessage;
import com.tieba.utils.ApiJsonResult;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 群消息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbGroupMessageService extends IService<TbGroupMessage> {
	
	/**
	 * Description: 发送一条消息<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:09:56 PM <br>
	 */
	public ApiJsonResult<?> sendMessage(TbGroupMessage tbGroupMessage);
	
	/**
	 * Description: 删除一条消息 <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param messageId
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:12:02 PM <br>
	 */
	public ApiJsonResult<?> delMessage(String messageId);
	
	/**
	 * Description: 拉取消息列表<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param pageParam
	 * @param tbGroupMessage
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:13:57 PM <br>
	 */
	public ApiJsonResult<?> queryMessageList(Page<TbGroupMessage> pageParam, TbGroupMessage tbGroupMessage);
	
	/**
	 * 查询指定群 最近的一条记录
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param groupId
	 * @return <br>
	 * @CreateDate Apr 15, 2020 10:57:19 PM <br>
	 */
	public TbGroupMessage queryCurrentMessage(String groupId);

}
