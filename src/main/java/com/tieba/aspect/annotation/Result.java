/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.aspect.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/** 
 * <Description> <br> 
 *  
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 19, 2020 10:05:19 PM <br>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Result {

	Class<? extends Throwable> exception();

    String msg();
}
