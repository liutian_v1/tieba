package com.tieba.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

/**
 * <Description> <br>
 * 
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 20, 2020 12:03:14 AM <br>
 */
@Aspect
@Component
public class LogInfoAspect {

	private static final Logger logger = LoggerFactory.getLogger(LogInfoAspect.class);

	@Before(value = "@within(com.tieba.aspect.annotation.LogInfo)")
	public void requestParams(JoinPoint joinPoint) {
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = joinPoint.getSignature().getName();
		Object[] args = joinPoint.getArgs();
		StringBuilder params = new StringBuilder();
		for (Object arg : args) {
			arg = JSON.toJSONString(arg);
			params.append(arg).append(" ");
		}
		logger.info("class: {}, methodName: {}, 入参为：{}",className, methodName, params);
	}
	
	@AfterReturning(value = "@within(com.tieba.aspect.annotation.LogInfo)", returning = "resultObj")
	public void responseParams(JoinPoint joinPoint, Object resultObj) {
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = joinPoint.getSignature().getName();
		String respParam = JSON.toJSONString(resultObj);
		logger.info("class: {}, methodName: {}, 出参为：{}",className, methodName, respParam);
	}

}
