/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.aspect;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.tieba.aspect.annotation.ErrorResult;
import com.tieba.aspect.annotation.Result;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/** 
 * <Description> <br> 
 *  
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 19, 2020 10:20:51 PM <br>
 */
@Aspect
@Component
public class ExcepitonAspect {
	
	/**
	 * 日志组件
	 */
	private static final Logger logger = LoggerFactory.getLogger(ExcepitonAspect.class);
	
	@Around("@within(com.tieba.aspect.annotation.EnableAutoResult)")
	public Object exception(ProceedingJoinPoint point) throws Throwable {
		try {
			Object proceed = point.proceed(point.getArgs());
            return proceed;
        } catch (Throwable e) {
        	print(e);
            MethodSignature signature = (MethodSignature) point.getSignature();
            Method method = signature.getMethod();
            if (method.getReturnType().equals(ApiJsonResult.class)) {
                ErrorResult errorResult = method.getAnnotation(ErrorResult.class);
                if (errorResult != null) {
                    for (Result result : errorResult.results()) {
                        if (e.getClass().isAssignableFrom(result.exception())) {
                            return R.wrong(result.msg());
                        }
                    }
                }
                return R.wrong("服务异常");
            } else {
                throw e;
            }
        }
	}
	
	private void print(Throwable e) {
        logger.error("服务调用异常： {}", getExceptionStack(e));
    }

    private String getExceptionStack(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        e.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }

}
