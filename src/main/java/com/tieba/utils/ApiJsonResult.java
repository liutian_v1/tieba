/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.utils;

import java.io.Serializable;

/**
 * <Description> <br>
 * 
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 19, 2020 9:18:35 PM <br>
 */

public class ApiJsonResult<T> implements Serializable {
	/**
	 * serialVersionUID <br>
	 */
	private static final long serialVersionUID = 1L;
	
	private Boolean success = true;
	private String msg = "";
	private T obj = null;
	private String code = "100";
	private String version = "1.0";
	private int size;

	public ApiJsonResult() {
	}

	public int getSize() {
		return this.size;
	}

	public ApiJsonResult<T> setSize(int size) {
		this.size = size;
		return this;
	}

	public Boolean getSuccess() {
		return this.success;
	}

	public ApiJsonResult<T> setSuccess(Boolean success) {
		this.success = success;
		return this;
	}

	public String getMsg() {
		return this.msg;
	}

	public ApiJsonResult<T> setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public T getObj() {
		return this.obj;
	}

	public ApiJsonResult<T> setObj(T object) {
		this.obj = object;
		return this;
	}

	public String getCode() {
		return this.code;
	}

	public ApiJsonResult<T> setCode(String code) {
		this.code = code;
		return this;
	}

	public String getVersion() {
		return this.version;
	}

	public ApiJsonResult<T> setVersion(String version) {
		this.version = version;
		return this;
	}
}
