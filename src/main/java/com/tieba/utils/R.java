/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.utils;

/**
 * <Description> <br>
 * 
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 19, 2020 10:28:59 PM <br>
 */

public class R {

	public R() throws Throwable {
		throw new Throwable("该类禁止被初始化");
	}

	public static <T> ApiJsonResult<T> success() {
		return new ApiJsonResult<T>().setSuccess(true);
	}

	public static <T> ApiJsonResult<T> success(String msg) {
		return new ApiJsonResult<T>().setSuccess(true).setMsg(msg);
	}

	public static <T> ApiJsonResult<T> success(T obj, String msg) {
		return new ApiJsonResult<T>().setSuccess(true).setObj(obj).setMsg(msg);
	}
	
	public static <T> ApiJsonResult<T> success(T obj, String msg, int total) {
		return new ApiJsonResult<T>().setSuccess(true).setObj(obj).setMsg(msg).setSize(total);
	}

	public static <T> ApiJsonResult<T> fail() {
		return new ApiJsonResult<T>().setSuccess(false);
	}

	public static <T> ApiJsonResult<T> fail(String msg) {
		return new ApiJsonResult<T>().setSuccess(false).setMsg(msg);
	}

	public static <T> ApiJsonResult<T> fail(T obj, String msg) {
		return new ApiJsonResult<T>().setSuccess(false).setObj(obj).setMsg(msg);
	}

	public static <T> ApiJsonResult<T> wrong() {
		return new ApiJsonResult<T>().setCode("500").setSuccess(false);
	}

	public static <T> ApiJsonResult<T> wrong(String msg) {
		return new ApiJsonResult<T>().setCode("500").setSuccess(false).setMsg(msg);
	}

	public static <T> ApiJsonResult<T> wrong(T obj, String msg) {
		return new ApiJsonResult<T>().setCode("500").setSuccess(false).setObj(obj).setMsg(msg);
	}
}
