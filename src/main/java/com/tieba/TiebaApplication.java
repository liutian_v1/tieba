/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba;

import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/** 
 * <Description> <br> 
 *  
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 17, 2020 10:16:33 AM <br>
 */
@SpringBootApplication
public class TiebaApplication {

	/**
	 * 日志组件
	 */
	private static final Logger logger = LoggerFactory.getLogger(TiebaApplication.class);

    public static void main(String[] args) {
        logger.info("启动贴吧系统");
        System.setProperty("test.timezone", "GMT +08");
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
        SpringApplication.run(TiebaApplication.class);
    }
}
