package com.tieba.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 收藏表
 * </p>
 *
 * @author lt
 * @since 2020-03-27
 */
public class TbCollect implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 收藏id
     */
    @TableId(type = IdType.UUID)
    private String collectId;

    /**
     * 帖子id
     */
    private String postId;

    /**
     * 收藏时间
     */
    private String createTime;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 帖子名称
     */
    @TableField(exist = false)
    private TbPostInfo tbPostInfo;

    public TbPostInfo getTbPostInfo() {
        return tbPostInfo;
    }

    public void setTbPostInfo(TbPostInfo tbPostInfo) {
        this.tbPostInfo = tbPostInfo;
    }

    public String getCollectId() {
        return collectId;
    }

    public void setCollectId(String collectId) {
        this.collectId = collectId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



	@Override
    public String toString() {
        return "TbCollect{" +
        "collectId=" + collectId +
        ", postId=" + postId +
        ", createTime=" + createTime +
        ", userId=" + userId +
        "}";
    }
}
