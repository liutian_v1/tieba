package com.tieba.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 群消息表
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public class TbGroupMessage implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 群消息id
     */
    @TableId(type = IdType.UUID)
    private String groupMessageId;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 发送者用户id
     */
    private String userId;
    /**
     * 发送者名称
     */
    @TableField(exist = false)
    private String userName;
    /**
     * 群id
     */
    private String groupId;


    public String getGroupMessageId() {
        return groupMessageId;
    }

    public void setGroupMessageId(String groupMessageId) {
        this.groupMessageId = groupMessageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
    
    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
    public String toString() {
        return "TbGroupMessage{" +
        "groupMessageId=" + groupMessageId +
        ", message=" + message +
        ", createTime=" + createTime +
        ", userId=" + userId +
        ", groupId=" + groupId +
        "}";
    }
}
