package com.tieba.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 点赞表
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public class TbLike implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 点赞id
     */
    @TableId(type = IdType.UUID)
    private String likeId;

    /**
     * 评论id或者帖子id
     */
    private String postOrComtId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 1-帖子，2-评论
     */
    private Integer likeType;


    public String getLikeId() {
        return likeId;
    }

    public void setLikeId(String likeId) {
        this.likeId = likeId;
    }

    public String getPostOrComtId() {
        return postOrComtId;
    }

    public void setPostOrComtId(String postOrComtId) {
        this.postOrComtId = postOrComtId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getLikeType() {
        return likeType;
    }

    public void setLikeType(Integer likeType) {
        this.likeType = likeType;
    }

    @Override
    public String toString() {
        return "TbLike{" +
        "likeId=" + likeId +
        ", postOrComtId=" + postOrComtId +
        ", userId=" + userId +
        ", likeType=" + likeType +
        "}";
    }
}
