package com.tieba.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 帖子表
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public class TbPostInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 帖子id
     */
    @TableId(type = IdType.UUID)
    private String postId;

    /**
     * 帖子标题
     */
    private String postTitle;

    /**
     * 帖子内容
     */
    private String postContent;

    /**
     * 发布时间
     */
    private String createTime;

    /**
     * 发布人id
     */
    private String postUserId;

    /**
     * 帖子状态
     */
    private Integer postStuatus;

    /**
     * 是否置顶
     */
    private Integer isTop;

    /**
     * 阅读量
     */
    private Long readCount;

    /**
     * 帖子类型
     */
    private Integer postType;
    
    /**
     * 用户姓名
     */
    @TableField(exist = false)
    private String userName;
    
    @TableField(exist = false)
    private String headImageUrl;
    
    

    /** 
	 * get headImageUrl
	 * @return Returns the headImageUrl.<br> 
	 */
	public String getHeadImageUrl() {
		return headImageUrl;
	}

	/** 
	 * set headImageUrl
	 * @param headImageUrl The headImageUrl to set. <br>
	 */
	public void setHeadImageUrl(String headImageUrl) {
		this.headImageUrl = headImageUrl;
	}

	/** 
	 * get userName
	 * @return Returns the userName.<br> 
	 */
	public String getUserName() {
		return userName;
	}

	/** 
	 * set userName
	 * @param userName The userName to set. <br>
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPostUserId() {
        return postUserId;
    }

    public void setPostUserId(String postUserId) {
        this.postUserId = postUserId;
    }

    public Integer getPostStuatus() {
        return postStuatus;
    }

    public void setPostStuatus(Integer postStuatus) {
        this.postStuatus = postStuatus;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Long getReadCount() {
        return readCount;
    }

    public void setReadCount(Long readCount) {
        this.readCount = readCount;
    }

    public Integer getPostType() {
        return postType;
    }

    public void setPostType(Integer postType) {
        this.postType = postType;
    }

    @Override
    public String toString() {
        return "TbPostInfo{" +
        "postId=" + postId +
        ", postTitle=" + postTitle +
        ", postContent=" + postContent +
        ", createTime=" + createTime +
        ", postUserId=" + postUserId +
        ", postStuatus=" + postStuatus +
        ", isTop=" + isTop +
        ", readCount=" + readCount +
        ", postType=" + postType +
        "}";
    }
}
