package com.tieba.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 群人员表
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public class TbGroupUser implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(type = IdType.UUID)
    private String groupUserId;

    /**
     * 群id
     */
    private String groupId;
    /**
     * 群名称
     */
    @TableField(exist = false)
    private String groupName;

    /**
     * 用户id
     */
    private String userId;
    
    /**
     * 加群时间
     */
    private String createtime;
    
    @TableField(exist = false)
    private String message;

    /**
     * 群用户类型:1-创建者，2-管理员，3-普通用户
     */
    private Integer groupUserType;


    public String getGroupUserId() {
        return groupUserId;
    }

    public void setGroupUserId(String groupUserId) {
        this.groupUserId = groupUserId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getGroupUserType() {
        return groupUserType;
    }

    public void setGroupUserType(Integer groupUserType) {
        this.groupUserType = groupUserType;
    }
    

    /** 
	 * get createTime
	 * @return Returns the createTime.<br> 
	 */
	public String getCreateTime() {
		return createtime;
	}

	/** 
	 * set createTime
	 * @param createTime The createTime to set. <br>
	 */
	public void setCreateTime(String createTime) {
		this.createtime = createTime;
	}
	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
    public String toString() {
        return "TbGroupUser{" +
        "groupUserId=" + groupUserId +
        ", groupId=" + groupId +
        ", userId=" + userId +
         ", createTime=" + createtime +
        ", groupUserType=" + groupUserType +
        "}";
    }
}
