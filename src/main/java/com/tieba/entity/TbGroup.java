package com.tieba.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 评论状态
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public class TbGroup implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 群id
     */
    @TableId(type = IdType.UUID)
    private String groupId;

    /**
     * 群名称
     */
    private String groupName;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 群状态
     */
    private Integer groupStatus;

    /**
     * 群类型
     */
    private Integer groupType;

    /**
     * 创建人
     */
    private String userId;
    @TableField(exist = false)
    private String userName;


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(Integer groupStatus) {
        this.groupStatus = groupStatus;
    }

    public Integer getGroupType() {
        return groupType;
    }

    public void setGroupType(Integer groupType) {
        this.groupType = groupType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
    public String toString() {
        return "TbGroup{" +
        "groupId=" + groupId +
        ", groupName=" + groupName +
        ", createTime=" + createTime +
        ", groupStatus=" + groupStatus +
        ", groupType=" + groupType +
        ", userId=" + userId +
        "}";
    }
}
