package com.tieba.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 评论表
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public class TbCommentInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 评论id
     */
    @TableId(type = IdType.UUID)
    private String commentId;

    /**
     * 帖子id
     */
    private String postId;

    /**
     * 评论时间
     */
    private String commentTime;

    /**
     * 评论内容
     */
    private String commentContent;

    /**
     * 评论用户id
     */
    private String userId;
    
    /**
     * 被回复用户id
     */
    private String toUserId;

    /**
     * 评论上级id
     */
    private String commentParentId;

    /**
     * 评论状态
     */
    private Integer commentStatus;
    
    @TableField(exist = false)
    private String userName;
    
    @TableField(exist = false)
    private String headImageUrl;
    
    @TableField(exist = false)
    private String toUserName;
    
    @TableField(exist = false)
    private String toHeadImageUrl;
    
    


    /** 
	 * get userName
	 * @return Returns the userName.<br> 
	 */
	public String getUserName() {
		return userName;
	}

	/** 
	 * set userName
	 * @param userName The userName to set. <br>
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/** 
	 * get headImageUrl
	 * @return Returns the headImageUrl.<br> 
	 */
	public String getHeadImageUrl() {
		return headImageUrl;
	}

	/** 
	 * set headImageUrl
	 * @param headImageUrl The headImageUrl to set. <br>
	 */
	public void setHeadImageUrl(String headImageUrl) {
		this.headImageUrl = headImageUrl;
	}

	public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCommentParentId() {
        return commentParentId;
    }

    public void setCommentParentId(String commentParentId) {
        this.commentParentId = commentParentId;
    }

    public Integer getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(Integer commentStatus) {
        this.commentStatus = commentStatus;
    }

    /** 
	 * get toUserId
	 * @return Returns the toUserId.<br> 
	 */
	public String getToUserId() {
		return toUserId;
	}

	/** 
	 * set toUserId
	 * @param toUserId The toUserId to set. <br>
	 */
	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}

	/** 
	 * get toUserName
	 * @return Returns the toUserName.<br> 
	 */
	public String getToUserName() {
		return toUserName;
	}

	/** 
	 * set toUserName
	 * @param toUserName The toUserName to set. <br>
	 */
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	/** 
	 * get toHeadImageUrl
	 * @return Returns the toHeadImageUrl.<br> 
	 */
	public String getToHeadImageUrl() {
		return toHeadImageUrl;
	}

	/** 
	 * set toHeadImageUrl
	 * @param toHeadImageUrl The toHeadImageUrl to set. <br>
	 */
	public void setToHeadImageUrl(String toHeadImageUrl) {
		this.toHeadImageUrl = toHeadImageUrl;
	}

	@Override
    public String toString() {
        return "TbCommentInfo{" +
        "commentId=" + commentId +
        ", postId=" + postId +
        ", commentTime=" + commentTime +
        ", commentContent=" + commentContent +
        ", userId=" + userId +
        ", commentParentId=" + commentParentId +
        ", commentStatus=" + commentStatus +
        "}";
    }
}
