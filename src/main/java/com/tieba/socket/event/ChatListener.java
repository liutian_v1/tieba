/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.socket.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.BroadcastOperations;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.tieba.dao.TestDao;
import com.tieba.entity.TbGroupMessage;
import com.tieba.entity.TbUser;
import com.tieba.service.TbGroupMessageService;
import com.tieba.service.TbUserService;

/**
 * <Description> 聊天监听器<br>
 * 
 * @author liutian<br>
 * @version 1.0<br>
 * @param <T>
 * @taskId <br>
 * @CreateDate Apr 13, 2020 10:46:52 PM <br>
 */
@Component
public class ChatListener {

	@Autowired
	private SocketIOServer socketIoServer;
	
	@Autowired
	private TbGroupMessageService tbGroupMessageService;
	
	@Autowired
	private TbUserService tbUserService;
	
	@Autowired
	private TestDao testDao;
	

	/**
	 * Description: <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param client
	 * @param data
	 * @param ackSender <br>
	 * @CreateDate Apr 13, 2020 11:04:10 PM <br>
	 */
	@OnEvent(value = "sendMsg")
	public void sendMsg(SocketIOClient socketIOClient, AckRequest ackRequest, JSONObject msg) {
		String userId = msg.getString("userId");
		String groupId = msg.getString("groupId");
		String room = "group_" + groupId;
		BroadcastOperations roomOperations = socketIoServer.getRoomOperations(room);
		if (roomOperations.getClients().size() == 0) {
			return;
		}
		TbUser byId = tbUserService.getById(userId);
		String querySystime = testDao.querySystime();
		msg.put("userName", byId.getUserName());
		msg.put("time", querySystime);
		pushGroupMessage(socketIOClient, ackRequest, msg);
		// 保存消息到表里面
		TbGroupMessage tbGroupMessage =  new TbGroupMessage();
		tbGroupMessage.setGroupId(groupId);
		String message = msg.getString("msg");
		tbGroupMessage.setMessage(message);
		tbGroupMessage.setUserId(userId);
		tbGroupMessageService.save(tbGroupMessage);
	}

	/**
	 * Description: 根据群id推送群消息 <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param socketIOClient
	 * @param ackRequest
	 * @param msg            <br>
	 * @CreateDate Apr 14, 2020 10:40:31 AM <br>
	 */
	@OnEvent(value = "pushGroupMessage")
	public void pushGroupMessage(SocketIOClient socketIOClient, AckRequest ackRequest, JSONObject msg) {
		String groupId = msg.getString("groupId");
		String room = "group_" + groupId;
		BroadcastOperations roomOperations = socketIoServer.getRoomOperations(room);
		if (roomOperations.getClients().size() == 0) {
			return;
		}
		roomOperations.sendEvent("groupMsgList", msg);
	}

}
