package com.tieba.dao;

import com.tieba.entity.TbGroupMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 群消息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbGroupMessageMapper extends BaseMapper<TbGroupMessage> {

}
