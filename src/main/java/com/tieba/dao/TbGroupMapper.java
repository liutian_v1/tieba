package com.tieba.dao;

import com.tieba.entity.TbGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论状态 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbGroupMapper extends BaseMapper<TbGroup> {

}
