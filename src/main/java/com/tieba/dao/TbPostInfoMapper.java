package com.tieba.dao;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tieba.entity.TbPostInfo;

/**
 * <p>
 * 帖子表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbPostInfoMapper extends BaseMapper<TbPostInfo> {
	
	/**
	 * Description: 帖子增加阅读量 <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param postId
	 * @return <br>
	 * @CreateDate Mar 24, 2020 9:07:27 PM <br>
	 */
	public int addReadCount(@Param("postId")String postId);

}
