package com.tieba.dao;

import com.tieba.entity.TbCommentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbCommentInfoMapper extends BaseMapper<TbCommentInfo> {

}
