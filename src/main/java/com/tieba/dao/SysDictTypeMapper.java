package com.tieba.dao;

import com.tieba.entity.SysDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
