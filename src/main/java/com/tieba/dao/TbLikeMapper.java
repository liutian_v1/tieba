package com.tieba.dao;

import com.tieba.entity.TbLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 点赞表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbLikeMapper extends BaseMapper<TbLike> {

}
