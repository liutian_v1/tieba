package com.tieba.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tieba.entity.TbCollect;

/**
 * <p>
 * 收藏表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-27
 */
public interface TbCollectMapper extends BaseMapper<TbCollect> {

}
