package com.tieba.dao;

import com.tieba.entity.TbGroupUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 群人员表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
public interface TbGroupUserMapper extends BaseMapper<TbGroupUser> {

}
