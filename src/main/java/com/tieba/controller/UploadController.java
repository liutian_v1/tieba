package com.tieba.controller;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.tieba.TiebaApplication;
import com.tieba.common.utils.UploadUtils;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

@Controller
@RequestMapping(value = "upload")
public class UploadController {
	private static final Logger logger = LoggerFactory.getLogger(UploadController.class);
	
	@Value("${uplaod.imagesUrl}")
	private String uploadDir;
	// 跳转到图片上传界面
	@GetMapping("/toUploadImage")
	public String toUploadImage() {
		return "upload/uploadImage";
	}

	@PostMapping("/uploadImage")
	@ResponseBody
	public ApiJsonResult<?> uploadImage(@RequestParam("file") MultipartFile file) {
		ApiJsonResult<?> result;
		try {
			// 图片路径
			String imgUrl = null;
			// 上传
			String filename = UploadUtils.upload(file, uploadDir, file.getOriginalFilename());
			if (filename != null) {
				imgUrl = uploadDir+ "/" + filename;
			}
			result = R.success(uploadDir+"/"+filename, "成功");
		} catch (Exception e) {
			e.printStackTrace();
			result = R.fail("上传失败");
		}
		return result;
	}

	@RequestMapping(value = "showImg")
	public void ShowImg(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String imgFile = request.getParameter("imgFile"); // 文件名
		logger.error("图像文件：" + imgFile);
		FileInputStream fileIs = null;
		try {
			//fileIs = new FileInputStream(uploadDir + "/" + imgFile);
			fileIs = new FileInputStream(imgFile);
		} catch (Exception e) {
			logger.error("系统找不到图像文件：" + uploadDir + "/" + imgFile);
		}
//		finally {
//			if(null==fileIs) {
//				imgFile=System.getProperty("user.dir")+"/src/main/resources/static/images/head/head_mr.png";
//				System.out.println("返回的路径："+System.getProperty("user.dir")+"/src/main/resources/static/images/head/head_mr.png"); 
//				fileIs = new FileInputStream(imgFile);
//			}
//		}
		int i = fileIs.available(); // 得到文件大小
		byte data[] = new byte[i];
		fileIs.read(data); // 读数据
		response.setContentType("image/*"); // 设置返回的文件类型
		OutputStream outStream = response.getOutputStream(); // 得到向客户端输出二进制数据的对象
		outStream.write(data); // 输出数据
		outStream.flush();
		outStream.close();
		fileIs.close();
	}
}
