package com.tieba.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tieba.common.utils.PageUtils;
import com.tieba.common.utils.UserContext;
import com.tieba.common.utils.web.AjaxResult;
import com.tieba.entity.SysDictType;
import com.tieba.entity.TbCollect;
import com.tieba.entity.TbGroup;
import com.tieba.entity.TbPostInfo;
import com.tieba.entity.TbUser;
import com.tieba.service.TbGroupService;
import com.tieba.service.TbGroupUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

/**
 * 群组控制类
 * <Description> <br> 
 *  
 * @author mac<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Apr 15, 2020 10:09:22 PM <br>
 */
@Controller
@RequestMapping("/tbGroup")
public class TbGroupController extends BaseController {
	
	private String prefix = "bgmt/group/";
	@Autowired
	private TbGroupService tbGroupService;
	@Autowired
	private TbGroupUserService tbGroupUserService;
	
	@RequestMapping("/queryList")
	@ResponseBody
	public ApiJsonResult<?> queryList(@ModelAttribute TbGroup tbGroup, HttpServletRequest request) {
		Page<TbGroup> pageParam = PageUtils.convertPage(request);
		return tbGroupService.selectGroupInfo(pageParam, tbGroup);
	}
	
	/**
	 * 删除
	 */
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		TbUser tbUser=UserContext.getCurreentUser();
		ApiJsonResult<?> result=tbGroupService.delGroup(ids, tbUser.getUserId());
		return toAjax(result.getSuccess());
	}
	/**
	 * 修改群组
	 */
	@GetMapping("/edit/{uuid}")
	public String edit(@PathVariable("uuid") String uuid, ModelMap mmap) {
		//查询群组消息
		mmap.put("groupInfo", tbGroupUserService.queryGroupById(uuid).getObj());
		return prefix + "edit";
	}
}

