package com.tieba.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tieba.common.utils.UserContext;
import com.tieba.entity.TbGroup;
import com.tieba.entity.TbGroupMessage;
import com.tieba.entity.TbUser;
import com.tieba.service.TbGroupMessageService;
import com.tieba.service.TbGroupService;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 群消息表 前端控制器
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Controller
@RequestMapping("/tbGroupMessage")
public class TbGroupMessageController {
	@Autowired
	private TbGroupMessageService tbGroupMessageService;
	@Autowired
	private TbGroupService tbGroupService;
	@Autowired
	private TbUserService tbUserService;
	/**
	 * 查询消息会话组
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param userId
	 * @return <br>
	 * @CreateDate Apr 15, 2020 10:10:57 PM <br>
	 */
	@RequestMapping("/queryGroupMessageInfo")
	@ResponseBody
	public ApiJsonResult<?> queryGroupInfo(String groupId,String userId) {
		TbGroupMessage groupMessage=new TbGroupMessage();
		groupMessage.setGroupId(groupId);
		groupMessage.setUserId(userId);
		Page<TbGroupMessage> pageParam=new Page<TbGroupMessage>();
		ApiJsonResult<?> groupUser=tbGroupMessageService.queryMessageList(pageParam, groupMessage);
		return groupUser;
	}
	/**
	 * 添加群组人员操作界面
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param request
	 * @param response
	 * @return <br>
	 * @CreateDate Apr 21, 2020 9:50:48 PM <br>
	 */
	@GetMapping("/addGroupMessage")
	public String addGroupMessage(HttpServletRequest request, HttpServletResponse response) {
		return "message/addGroupMessage";
	}
	@RequestMapping("/queryUserInfo")
	@ResponseBody
	public ApiJsonResult<?> queryUserInfo(HttpServletRequest request, HttpServletResponse response) {
		TbUser tbUser=new TbUser();
		ApiJsonResult<?> queryAllUser = tbUserService.queryAllUser(tbUser);
		return queryAllUser;
	}
	@RequestMapping("/creaetGroupUser")
	@ResponseBody
	public ApiJsonResult<?> creaetGroupUser(String groupName,String groupInfo) {
		System.out.println("进入这家群组页面");
		TbGroup tbGroup=new TbGroup();
		tbGroup.setGroupName(groupName);
		return tbGroupService.createGroupAndUserInfo(tbGroup, groupInfo);
	}
}

