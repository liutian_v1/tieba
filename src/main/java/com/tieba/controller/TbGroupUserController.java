package com.tieba.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tieba.common.utils.UserContext;
import com.tieba.entity.TbCollect;
import com.tieba.entity.TbUser;
import com.tieba.service.TbGroupService;
import com.tieba.service.TbGroupUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 群人员表 前端控制器
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Controller
@RequestMapping("/tbGroupUser")
public class TbGroupUserController {
	@Autowired
	private TbGroupUserService tbGroupUserService;
	/**
	 * 查询消息会话组
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param userId
	 * @return <br>
	 * @CreateDate Apr 15, 2020 10:10:57 PM <br>
	 */
	@RequestMapping("/queryGroupInfo")
	@ResponseBody
	public ApiJsonResult<?> queryGroupInfo(String userId) {
		TbUser curreentUser = UserContext.getCurreentUser();
		if (curreentUser == null) {
			return R.fail("请登录");
		}
		ApiJsonResult<?> groupUser=tbGroupUserService.queryGroupUserById(curreentUser.getUserId());
		return groupUser;
	}
}

