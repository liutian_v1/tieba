package com.tieba.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tieba.common.utils.PageUtils;
import com.tieba.common.utils.UserContext;
import com.tieba.entity.TbCommentInfo;
import com.tieba.entity.TbUser;
import com.tieba.service.TbCommentInfoService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 评论表 前端控制器
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@RestController
@RequestMapping("/tbCommentInfo")
public class TbCommentInfoController {
	
	@Autowired
	private TbCommentInfoService tbCommentInfoService;
	
	/**
	 * Description: 根据帖子id获取评论信息列表<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param request
	 * @return <br>
	 * @CreateDate Mar 22, 2020 9:15:56 PM <br>
	 */
	@RequestMapping("/getCommentListByPostId")
	public ApiJsonResult<?> getCommentListByPostId(HttpServletRequest request) {
		String postId = request.getParameter("postId");
		Page<TbCommentInfo> paramPage = PageUtils.convertPage(request);
		return tbCommentInfoService.getCommentByPostId(postId, paramPage);
	}
	
	/**
	 * Description: 根据帖子id,评论id获取评论详情信息列表<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param request
	 * @return <br>
	 * @CreateDate Mar 22, 2020 9:15:56 PM <br>
	 */
	@RequestMapping("/getCommentByPostIdAndComId")
	public ApiJsonResult<?> getCommentByPostIdAndComId(HttpServletRequest request) {
		String postId = request.getParameter("postId");
		String commentId = request.getParameter("commentId");
		Page<TbCommentInfo> paramPage = PageUtils.convertPage(request);
		return tbCommentInfoService.getCommentByPostIdAndComId(postId, commentId, paramPage);
	}
	/**
	 * 根据帖子 id 评论帖子
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param request
	 * @return <br>
	 * @CreateDate Mar 31, 2020 11:01:21 PM <br>
	 */
	@RequestMapping("/followCommentByPostId")
	public ApiJsonResult<?> followCommentByPostId(TbCommentInfo info) {
		TbUser user=UserContext.getCurreentUser();
		if(null==user) {
			return R.wrong("未登录，请先登录");
		}
		info.setUserId(user.getUserId());
		info.setUserName(user.getUserName());
		return tbCommentInfoService.addComment(info);
	}
	/**
	 * 
	 * Description: <br> 
	 *  
	 * @author hbj<br>
	 * @taskId <br>
	 * @param info
	 * @return <br>
	 * @CreateDate Apr 4, 2020 2:54:19 PM <br>
	 */
	@RequestMapping("/publishedComment")
	public ApiJsonResult<?> publishedComment(TbCommentInfo info) {
		TbUser user=UserContext.getCurreentUser();
		if(null==user) {
			return R.wrong("未登录，请先登录");
		}
		info.setUserId(user.getUserId());
		info.setUserName(user.getUserName());
		return tbCommentInfoService.addItemComment(info);
	}
}

