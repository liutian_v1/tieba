package com.tieba.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tieba.common.utils.ServletUtils;
import com.tieba.common.utils.TiebaEnum;
import com.tieba.common.utils.UserContext;
import com.tieba.entity.TbUser;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

@Controller
public class LoginController {
	@Autowired
	private TbUserService tbUserService;
	@GetMapping("/login")
	public String login(HttpServletRequest request, HttpServletResponse response) {
		// 如果是Ajax请求，返回Json字符串。
		if (ServletUtils.isAjaxRequest(request)) {
			ApiJsonResult<?> result=R.wrong("用户请求不合法");
			return String.valueOf(result);
		}

		return "login/login";
	}
	@PostMapping("/toMian")
	@ResponseBody
	public ApiJsonResult<?> ajaxLogin(String username, String password) {
    	ApiJsonResult<?> like = tbUserService.queryTbUser(username, password);
        return like;
	}
	/**
	 * 返回主页
	 * Description: <br> 
	 * @author lt<br>
	 * @taskId <br>
	 * @param request
	 * @param response
	 * @param model
	 * @return <br>
	 * @CreateDate 2020年3月22日 上午12:40:17 <br>
	 */
	@GetMapping("/post/main")
	public String main(HttpServletRequest request, HttpServletResponse response,Model model){
		//用户信息传递到
		model.addAttribute("logininfo", UserContext.getCurreentUser());
		if(null!=UserContext.getCurreentUser()) {
			model.addAttribute("loginFlag","loginFlag");
		}
		return "post/main";
	}
	/**
	 * 返回注册页面
	 */
	@GetMapping("/register")
	public String register(HttpServletRequest request, HttpServletResponse response,Model model){
		return "login/register";
	}
	/**
	 * 注册用户
	 * Description: <br> 
	 * @author lt<br>
	 * @taskId <br>
	 * @param request
	 * @param response
	 * @return <br>
	 * @CreateDate 2020年3月22日 上午12:39:40 <br>
	 */
	@PostMapping("/saveRegister")
	@ResponseBody
	public ApiJsonResult<?> saveRegister(HttpServletRequest request,HttpServletResponse response){
		TbUser user=new TbUser();
		user.setLoginName(request.getParameter("loginName"));
		user.setPassword(request.getParameter("password"));
		user.setEmail(request.getParameter("email"));
		user.setUserName(request.getParameter("loginName"));
		user.setUserType(TiebaEnum.USER_TYPE_USER.getType());
		user.setStatus(1);
		ApiJsonResult<?>  saveUserInfo =this.tbUserService.registerUserInfo(user);
		return saveUserInfo;
	}
	/**
	 * 修改个人信息
	 */
	@GetMapping("/updateUserInfo")
	public String updateUserInfo(HttpServletRequest request, HttpServletResponse response,Model model){
		model.addAttribute("logininfo", UserContext.getCurreentUser());
		return "login/userInfo";
	}
}
