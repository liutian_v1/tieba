package com.tieba.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tieba.common.utils.PageUtils;
import com.tieba.common.utils.UserContext;
import com.tieba.entity.TbCollect;
import com.tieba.entity.TbUser;
import com.tieba.service.TbCollectService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 收藏表 前端控制器
 * </p>
 *
 * @author lt
 * @since 2020-03-27
 */
@Controller
@RequestMapping("/tbCollect")
public class TbCollectController {

	@Autowired
	private TbCollectService tbCollectService;

	/**
	 * Description: 添加收藏接口<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:46:55 PM <br>
	 */
	@RequestMapping("/addCollect")
	@ResponseBody
	public ApiJsonResult<?> addCollect(String postId) {
		TbUser curreentUser = UserContext.getCurreentUser();
		if (curreentUser == null) {
			return R.fail("请登录");
		}
		TbCollect tbCollect = new TbCollect();
		tbCollect.setPostId(postId);
		tbCollect.setUserId(curreentUser.getUserId());
		return tbCollectService.addCollect(tbCollect);
	}

	/**
	 * Description: 取消收藏<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param collectId
	 * @return <br>
	 * @CreateDate Mar 27, 2020 10:51:57 PM <br>
	 */
	@RequestMapping("/cancelCollect")
	@ResponseBody
	public ApiJsonResult<?> cancelCollect(String collectId) {
		if (StringUtils.isBlank(collectId)) {
			return R.fail("请填写收藏id");
		}
		return tbCollectService.cancleCollect(collectId);
	}
	
	/**
	 * Description: 查询收藏列表<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Apr 7, 2020 10:31:18 PM <br>
	 */
	@RequestMapping("/queryCollectList")
	@ResponseBody
	public ApiJsonResult<?> queryCollectList(HttpServletRequest request) {
		TbUser curreentUser = UserContext.getCurreentUser();
		if (curreentUser == null) {
			return R.fail("请登录");
		}
		Page<TbCollect> convertPage = PageUtils.convertPage(request);
		TbCollect tbCollect = new TbCollect();
		tbCollect.setUserId(curreentUser.getUserId());
		return tbCollectService.queryCollectList(convertPage, tbCollect);
	}

}
