package com.tieba.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tieba.common.utils.PageUtils;
import com.tieba.common.utils.TiebaEnum;
import com.tieba.common.utils.UserContext;
import com.tieba.entity.TbPostInfo;
import com.tieba.entity.TbUser;
import com.tieba.service.TbPostInfoService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 帖子表 前端控制器
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Controller
@RequestMapping("/tbPostInfo")
public class TbPostInfoController {
	
	@Autowired
	private TbPostInfoService tbPostInfoService;
	
	/**
	 * 发布贴子页面
	 * Description: <br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @param request
	 * @param response
	 * @param model
	 * @return <br>
	 * @CreateDate 2020年3月23日 下午8:03:35 <br>
	 */
	@GetMapping("/published")
	public String register(HttpServletRequest request, HttpServletResponse response,Model model){
		return "post/published";
	}
	/**
	 * 返回贴吧主页
	 * Description: <br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @param request
	 * @param response
	 * @param model
	 * @return <br>
	 * @CreateDate 2020年3月23日 下午8:03:35 <br>
	 */
	@GetMapping("/main")
	public String main(HttpServletRequest request, HttpServletResponse response,Model model){
		return "post/main";
	}
	@GetMapping("/updatePostInfo")
	public String updatePostInfo(HttpServletRequest request, HttpServletResponse response,Model model){
		String postId=request.getParameter("postId");
		TbPostInfo tbPostInfo = tbPostInfoService.getById(postId);
		model.addAttribute("tbPostInfo",tbPostInfo);
		return "bgmt/updatePostInfo";
	}
	@GetMapping("/tiebadetails")
	public String tiebadetails(HttpServletRequest request, HttpServletResponse response,Model model){
		//获得用户信息
		model.addAttribute("logininfo", UserContext.getCurreentUser());
		String postId=request.getParameter("postId");
		//查询贴子详情
		ApiJsonResult<?> readPostInfo = tbPostInfoService.readPostInfo(postId);
		model.addAttribute("postDetails",readPostInfo.getObj());
		return "post/tiebadetails";
	}
	/**
	 * 查询贴吧列表
	 * Description: <br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate 2020年3月24日 下午7:58:50 <br>
	 */
	@PostMapping("/queryPostinfo")
	@ResponseBody
	public ApiJsonResult<?> queryPostinfo(HttpServletRequest request, HttpServletResponse response) {
		String searchValue=request.getParameter("searchValue");
		Page<TbPostInfo> paramPage=PageUtils.convertPage(request);
		TbPostInfo tbPostInfo=new TbPostInfo();
		if(StringUtils.isNotEmpty(searchValue)) {
			tbPostInfo.setPostTitle(searchValue);
		}
    	ApiJsonResult<?> postInfo = tbPostInfoService.queryPostList(paramPage, tbPostInfo);
        return postInfo;
	}
	/**
	 * 查询贴吧列表
	 * Description: <br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate 2020年3月24日 下午7:58:50 <br>
	 */
	@GetMapping("/queryUserPostinfo")
	@ResponseBody
	public String queryUserPostinfo(HttpServletRequest request, HttpServletResponse response) {
		String searchValue=request.getParameter("searchValue");
		Page<TbPostInfo> paramPage=PageUtils.convertPage(request);
		TbPostInfo tbPostInfo=new TbPostInfo();
		if(StringUtils.isNotEmpty(searchValue)) {
			tbPostInfo.setPostTitle(searchValue);
		}
    	return  tbPostInfoService.queryUserPostList(paramPage, tbPostInfo);
	}
	@PostMapping("/publishedSend")
	@ResponseBody
	public ApiJsonResult<?> publishedSend(HttpServletRequest request, HttpServletResponse response) {
		TbPostInfo tbPostInfo =new TbPostInfo();
		TbUser tbUser=UserContext.getCurreentUser();
		if(null==tbUser) {
			return R.wrong("请先登录");
		}
		tbPostInfo.setPostTitle(request.getParameter("postTitle"));
		tbPostInfo.setPostContent(request.getParameter("postContent"));
		tbPostInfo.setUserName(tbUser.getUserName());
		tbPostInfo.setPostUserId(tbUser.getUserId());
		tbPostInfo.setIsTop(TiebaEnum.POST_NOT_ISTOP.getType());
		if(tbUser.getUserType()==TiebaEnum.POST_TYPE_GOV.getType()) {
			tbPostInfo.setPostType(TiebaEnum.POST_TYPE_GOV.getType());
		}else {
			tbPostInfo.setPostType(TiebaEnum.POST_TYPE_PALIN.getType());
		}
    	ApiJsonResult<?> postInfo = tbPostInfoService.savePost(tbPostInfo);
        return postInfo;
	}
	/**
	 * Description: 删除帖子<br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @param request
	 * @param response
	 * @return <br>
	 * @CreateDate Apr 16, 2020 10:59:07 PM <br>
	 */
	@PostMapping("/delPostById")
	@ResponseBody
	public ApiJsonResult<?> delPostById(HttpServletRequest request, HttpServletResponse response) {
		String postId=request.getParameter("postId");
    	ApiJsonResult<?> postInfo = tbPostInfoService.delPost(postId);
        return postInfo;
	}
	/**
	 * Description: 删除帖子<br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @param request
	 * @param response
	 * @return <br>
	 * @CreateDate Apr 16, 2020 10:59:07 PM <br>
	 */
	@PostMapping("/updatePostById")
	@ResponseBody
	public ApiJsonResult<?> updatePostById(HttpServletRequest request, HttpServletResponse response) {
		String postId=request.getParameter("postId");
		String postdealContent=request.getParameter("postContent");
		TbPostInfo tbPostInfo=new TbPostInfo();
		tbPostInfo.setPostId(postId);
		tbPostInfo.setPostContent(postdealContent);
    	ApiJsonResult<?> postInfo = tbPostInfoService.updatePost(tbPostInfo);
        return postInfo;
	}
	/**
	 * Description: 删除帖子<br> 
	 * @author hbj<br>
	 * @taskId <br>
	 * @param request
	 * @param response
	 * @return <br>
	 * @CreateDate Apr 16, 2020 10:59:07 PM <br>
	 */
	@PostMapping("/isTop")
	@ResponseBody
	public ApiJsonResult<?> isTop(HttpServletRequest request, HttpServletResponse response) {
		String postId=request.getParameter("postId");
		String isTop=request.getParameter("isTop");
		TbPostInfo tbPostInfo=new TbPostInfo();
		tbPostInfo.setPostId(postId);
		tbPostInfo.setIsTop(Integer.parseInt(isTop));
    	ApiJsonResult<?> postInfo = tbPostInfoService.updatePost(tbPostInfo);
        return postInfo;
	}
}

