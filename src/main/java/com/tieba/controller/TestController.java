/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tieba.service.TestService;

/** 
 * <Description> <br> 
 *  
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 17, 2020 10:23:04 AM <br>
 */
@Controller
@RequestMapping("/test")
public class TestController {
	
	@Autowired
	private TestService testService;
	
	/**
	 * Description: <br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Mar 17, 2020 10:39:11 AM <br>
	 */
	@RequestMapping("/getTime")
	@ResponseBody
	public String getTime() {
		return testService.getTime();
	}
	
	@RequestMapping("/login")
	public String tests() {
		return "login";
	}
	
	@RequestMapping("/testSocket")
	public String testSocket() {
		return "main/testSocket";
	}

}
