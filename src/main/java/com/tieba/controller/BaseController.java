package com.tieba.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tieba.common.utils.StringUtils;
import com.tieba.common.utils.web.AjaxResult;
import com.tieba.common.utils.web.AjaxResult.Type;

/**
 * web层通用数据处理
 * 
 * @author ruoyi
 */
public class BaseController {
    protected final Logger logger = LoggerFactory.getLogger(BaseController.class);
    
    /**
     * Description: 获取偏移量<br> 
     *  
     * @author lt<br>
     * @taskId <br>
     * @param <T>
     * @param request
     * @return <br>
     * @CreateDate Apr 1, 2020 10:01:33 PM <br>
     */
    public static <T> Page<T> convertPage(HttpServletRequest request) {
		String page = request.getParameter("pageNum");
		page = page != null && !"".equals(page) ? page : "1";
		String limit = request.getParameter("pageSize");
		limit = limit != null && !"".equals(limit) ? limit : "20";
		int pageInt = Integer.parseInt(page);
		int limitInt = Integer.parseInt(limit);
		Page<T> paramPage = new Page<>(pageInt, limitInt);
		return paramPage;
	}


    /**
     * 响应返回结果
     * 
     * @param rows 影响行数
     * @return 操作结果
     */
    protected AjaxResult toAjax(int rows)
    {
        return rows > 0 ? success() : error();
    }

    /**
     * 响应返回结果
     * 
     * @param result 结果
     * @return 操作结果
     */
    protected AjaxResult toAjax(boolean result)
    {
        return result ? success() : error();
    }

    /**
     * 返回成功
     */
    public AjaxResult success()
    {
        return AjaxResult.success();
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error()
    {
        return AjaxResult.error();
    }

    /**
     * 返回成功消息
     */
    public AjaxResult success(String message)
    {
        return AjaxResult.success(message);
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error(String message)
    {
        return AjaxResult.error(message);
    }

    /**
     * 返回错误码消息
     */
    public AjaxResult error(Type type, String message)
    {
        return new AjaxResult(type, message);
    }

    /**
     * 页面跳转
     */
    public String redirect(String url)
    {
        return StringUtils.format("redirect:{}", url);
    }
}
