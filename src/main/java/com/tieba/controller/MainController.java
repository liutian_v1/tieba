package com.tieba.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tieba.common.utils.ServletUtils;
import com.tieba.common.utils.UserContext;
import com.tieba.entity.TbUser;
import com.tieba.service.TbGroupService;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

@Controller
@RequestMapping(value = "main")
public class MainController {
    @Autowired
    private TbUserService tbUserService;
    @Autowired
    private TbGroupService tbGroupService;

    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request)) {
            ApiJsonResult<?> result = R.wrong("用户请求不合法");
            return String.valueOf(result);
        }

        return "login/login";
    }

    @GetMapping("/mainCenter")
    public String mainCenter(HttpServletRequest request, HttpServletResponse response) {
        return "post/mainCenter";
    }

    @GetMapping("/collection")
    public String collection(HttpServletRequest request, HttpServletResponse response) {
        return "post/collectionPage";
    }

    /**
     * 打开消息页面
     * Description: <br>
     *
     * @param request
     * @param response
     * @return <br>
     * @author hbj<br>
     * @taskId <br>
     * @CreateDate Apr 10, 2020 11:53:36 PM <br>
     */
    @GetMapping("/message")
    public String message(HttpServletRequest request, HttpServletResponse response, Model model) {
        model.addAttribute("logininfo", UserContext.getCurreentUser());
        return "message/main";
    }

    /**
     *
     */
    @GetMapping("/header")
    public String header(HttpServletRequest request, HttpServletResponse response) {
        return "message/Header";
    }

    @GetMapping("/life")
    public String life(HttpServletRequest request, HttpServletResponse response) {
        return "message/Life";
    }

    @GetMapping("/postUser")
    public String postUser(HttpServletRequest request, HttpServletResponse response) {
        return "bgmt/postUser";
    }

    @GetMapping("/right")
    public String right(HttpServletRequest request, HttpServletResponse response, Model model) {
        model.addAttribute("logininfo", UserContext.getCurreentUser());
        model.addAttribute("groupInfo", tbGroupService.getById(request.getParameter("groupId")));
        return "message/Right";
    }

    @GetMapping("/msgUser")
    public String msgUser(HttpServletRequest request, HttpServletResponse response) {
        return "message/msguser";
    }

    @GetMapping("/msgUserright")
    public String msgUserright(HttpServletRequest request, HttpServletResponse response) {
        return "message/right_user";
    }

    @GetMapping("/bgmtMian")
    public String bgmtMian(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (UserContext.getCurreentUser() == null) {//没有登录则 重指向登录界面
            return "redirect:/login";
        }
        model.addAttribute("logininfo", UserContext.getCurreentUser());
        return "bgmt/main";
    }

    @GetMapping("/bgmtPost")
    public String bgmtPost(HttpServletRequest request, HttpServletResponse response) {
        return "bgmt/postbgmt";
    }

    /**
     * 返回群组页面
     * Description: <br>
     *
     * @param request
     * @param response
     * @return <br>
     * @author hbj<br>
     * @taskId <br>
     * @CreateDate Apr 22, 2020 9:32:49 PM <br>
     */
    @GetMapping("/groupbgmt")
    public String groupbgmt(HttpServletRequest request, HttpServletResponse response) {
        return "bgmt/groupbgmt";
    }
}
