package com.tieba.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tieba.service.TbLikeService;
import com.tieba.utils.ApiJsonResult;

/**
 * <p>
 * 点赞表 前端控制器
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@RestController
@RequestMapping("/tbLike")
public class TbLikeController {
	
	/**
	 * 
	 */
	@Autowired
	private TbLikeService tbLikeService;
	
	/**
	 * Description: 点赞帖子<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param request
	 * @return <br>
	 * @CreateDate Mar 19, 2020 3:43:39 PM <br>
	 */
	@RequestMapping("/likePost")
	public ApiJsonResult<?> likePost(HttpServletRequest request) {
		String type = "1";
		String userId = request.getParameter("userId");
		String postOrComtId = request.getParameter("postOrComtId");
		ApiJsonResult<?> like = tbLikeService.like(userId, type, postOrComtId);
		return like;
	}
	
	/**
	 * Description: 点赞评论<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param request
	 * @return <br>
	 * @CreateDate Mar 19, 2020 4:03:38 PM <br>
	 */
	@RequestMapping("/likeComment")
	public ApiJsonResult<?> likeComment(HttpServletRequest request) {
		String type = "2";
		String userId = request.getParameter("userId");
		String postOrComtId = request.getParameter("postOrComtId");
		ApiJsonResult<?> like = tbLikeService.like(userId, type, postOrComtId);
		return like;
	}
	
	/**
	 * Description: 取消点赞<br> 
	 *  
	 * @author lt<br>
	 * @taskId <br>
	 * @param request
	 * @return <br>
	 * @CreateDate Mar 19, 2020 4:08:17 PM <br>
	 */
	@RequestMapping("/cancelLike")
	public ApiJsonResult<?> cancelLike(HttpServletRequest request) {
		String userId = request.getParameter("userId");
		String postOrComtId = request.getParameter("postOrComtId");
		ApiJsonResult<?> cancelLike = tbLikeService.cancelLike(userId, postOrComtId);
		return cancelLike;
	}
}

