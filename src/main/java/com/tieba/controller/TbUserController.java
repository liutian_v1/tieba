package com.tieba.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tieba.common.utils.PageUtils;
import com.tieba.common.utils.UserContext;
import com.tieba.common.utils.web.AjaxResult;
import com.tieba.entity.TbUser;
import com.tieba.service.TbUserService;
import com.tieba.utils.ApiJsonResult;
import com.tieba.utils.R;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Controller
@RequestMapping("/tbUser")
public class TbUserController extends BaseController {

	@Autowired
	private TbUserService tbUserService;

	private String prefix = "user";

	@PostMapping("/updateUser")
	@ResponseBody
	public ApiJsonResult<?> updateUserInfo(TbUser tbUser) {
		// 先确定图片位置或者路径是否变更
		// 确定用户数：
		// 之后执行以后信息的维护
		TbUser user = UserContext.getCurreentUser();
		if (null == user) {
			return R.wrong("用户信息已失效，请重新登录");
		}
		tbUser.setUserId(user.getUserId());
		tbUser.setUserType(user.getUserType());
		ApiJsonResult<?> result = tbUserService.updateUserInfo(tbUser);
		return result;
	}

	@GetMapping("/exit")
	public String exit(TbUser tbUser) {
		// 清空会话
		UserContext.removeUser();
		return "login/login";
	}

	/**
	 * 查询用户列表
	 */
	@PostMapping("/list")
	@ResponseBody
	public ApiJsonResult<?> list(TbUser tbUser, HttpServletRequest request) {
		Page<TbUser> paramPage = PageUtils.convertPage(request);
		ApiJsonResult<?> queryList = tbUserService.queryList(paramPage, tbUser);
		return queryList;
	}

	@GetMapping("/user")
	public String user() {
		return prefix + "/user";
	}

	/**
	 * 新增用户
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存用户
	 */
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(TbUser tbUser) {
		ApiJsonResult<?> registerUserInfo = tbUserService.registerUserInfo(tbUser);
		return toAjax(registerUserInfo.getSuccess());
	}

	/**
	 * 修改用户
	 */
	@GetMapping("/edit/{userId}")
	public String edit(@PathVariable("userId") String userId, ModelMap mmap) {
		TbUser tbUser = tbUserService.getById(userId);
		mmap.put("tbUser", tbUser);
		return prefix + "/edit";
	}

	/**
	 * 修改保存用户
	 */
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(TbUser tbUser) {
		return toAjax(tbUserService.updateById(tbUser));
	}

	/**
	 * 删除用户
	 */
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(tbUserService.removeByIds(Arrays.asList(ids.split(","))));
	}

}
