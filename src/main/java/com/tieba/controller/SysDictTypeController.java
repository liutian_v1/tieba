package com.tieba.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tieba.common.utils.PageUtils;
import com.tieba.common.utils.web.AjaxResult;
import com.tieba.entity.SysDictType;
import com.tieba.service.SysDictTypeService;
import com.tieba.utils.ApiJsonResult;

/**
 * <p>
 * 字典类型表 前端控制器
 * </p>
 *
 * @author lt
 * @since 2020-03-18
 */
@Controller
@RequestMapping("/sysDictType")
public class SysDictTypeController extends BaseController {

	@Autowired
	private SysDictTypeService sysDictService;

	private String prefix = "dict/";

	/**
	 * Description: <br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @param sysDict
	 * @return <br>
	 * @CreateDate Mar 30, 2020 11:31:47 PM <br>
	 */
	@RequestMapping("/queryList")
	@ResponseBody
	public ApiJsonResult<?> queryList(@ModelAttribute SysDictType sysDictType, HttpServletRequest request) {
		Page<SysDictType> sysDictPage = PageUtils.convertPage(request);
		return sysDictService.queryList(sysDictPage, sysDictType);
	}

	/**
	 * 新增字典属性
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "add";
	}

	@PostMapping("/addSave")
	@ResponseBody
	public AjaxResult addSave(SysDictType dict) {
		QueryWrapper<SysDictType> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("dict_name", dict.getDictName());
		queryWrapper.eq("dict_value", dict.getDictValue());
		queryWrapper.eq("dict_type", dict.getDictType());
		SysDictType one = sysDictService.getOne(queryWrapper);
		if (one != null) {
			return AjaxResult.error("改字典已经存在");
		}
		return toAjax(sysDictService.save(dict));
	}

	/**
	 * 修改会员
	 */
	@GetMapping("/edit/{uuid}")
	public String edit(@PathVariable("uuid") String uuid, ModelMap mmap) {
		SysDictType dict = sysDictService.getById(uuid);
		mmap.put("dict", dict);
		return prefix + "edit";
	}

	/**
	 * Description: 跳转到编辑页面<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Apr 1, 2020 12:13:18 AM <br>
	 */
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(SysDictType dict) {
		QueryWrapper<SysDictType> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("dict_name", dict.getDictName());
		queryWrapper.eq("dict_value", dict.getDictValue());
		queryWrapper.eq("dict_type", dict.getDictType());
		SysDictType one = sysDictService.getOne(queryWrapper);
		if (one != null) {
			return AjaxResult.error("改字典已经存在");
		}
		boolean saveOrUpdate = sysDictService.saveOrUpdate(dict);
		return toAjax(saveOrUpdate);
	}

	/**
	 * Description: 跳转到list页面<br>
	 * 
	 * @author lt<br>
	 * @taskId <br>
	 * @return <br>
	 * @CreateDate Apr 1, 2020 12:11:55 AM <br>
	 */
	@RequestMapping("/list")
	public String list() {
		return prefix + "dict";
	}

	/**
	 * 删除
	 */
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(sysDictService.removeByIds(Arrays.asList(ids.split(","))));
	}

}
