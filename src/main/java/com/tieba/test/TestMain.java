/**************************************************************************************** 
        绿蚁网络
 ****************************************************************************************/
package com.tieba.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.tieba.TiebaApplication;
import com.tieba.entity.TbPostInfo;
import com.tieba.service.TbLikeService;
import com.tieba.service.TbPostInfoService;
import com.tieba.utils.ApiJsonResult;

/** 
 * <Description> <br> 
 *  
 * @author liutian<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate Mar 19, 2020 10:35:42 PM <br>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TiebaApplication.class)
public class TestMain {
	
	@Autowired
	public TbLikeService tbLikeService;
	
	@Autowired
	public TbPostInfoService tbPostInfoService;
	
	@Test
	public void testExcepiton() {
		try {
//			ApiJsonResult<?> cancelLike = tbLikeService.cancelLike("123", "123");
//			for (int i=1000; i<10000; i++) {
//				System.out.println(i);
//				TbPostInfo tbPostInfo =  new TbPostInfo();
//				tbPostInfo.setPostTitle("帖子标题："+i);
//				tbPostInfo.setPostType(2);
//				tbPostInfo.setPostUserId("1");
//				tbPostInfo.setPostContent("帖子内容："+i);
//				tbPostInfoService.savePost(tbPostInfo);
//			}
			
			ApiJsonResult<?> readPostInfo = tbPostInfoService.readPostInfo("0011a7406efda1048a3ed215b0b6c0cb");
			System.out.println(JSON.toJSON(readPostInfo));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
